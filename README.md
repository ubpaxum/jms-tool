# JMS-Tool

JMS-Tool is set of utilities for message brokers (ActiveMQ, AMQBroker, SwiftMQ, etc.). 
It uses standard Java JMS API for browsing, consuming and sending messages.
It has 4 utilities:
 * jms-tool - tool to send, browse, delete messages in message broker
 * jms-listener - tool to listen for messages in specified queues and handle them (save, dump, ...)
 * jms-forwarder - tool to forward messages from one message broker/queue to another broker/queue
 * jms-shell - CLI (Command Line interface) with functionalities like jms-tool

## Author
Ibrahim Tarla (ubpaxum@gmail.com)

## Prerequisites
JDK 11+

## Configuration
All tools use configuration file in YAML format named jmstool.yaml (by default). 

Configuration files is search searched in the following order (if not specified in command line by `-c` option). 
 * current directory
 * user home directory ($HOME)
 * $JMS_TOOL_HOME - root of jms-tools is installation directory

Sample configuration
```yaml
connections:
  activemq:
    type: activemq
    url: tcp://localhost:61616
    userName: myuser
    userPassword: mypass
    origin: http://localhost:8161
    jolokiaBase: http://localhost:8161/console/jolokia
    pattern: ":address=\"(.[^\"]+)\""

  amq-broker:
    type: activemq
    url: tcp://bastion.a-consult.bg:31954
    userName: myuser
    userPassword: mypass
    origin: http://localhost:8161
    jolokiaBase: https://broker.apps.lab.gravis.bg/console/jolokia
    pattern: ":address=\"(.[^\"]+)\""

# Define forwarders
forwarders:
  amq-to-activemq:
    enabled: false
    listenConnection: amq-broker
    listenQueue: test.in.queue
    forwardConnection: activemq
    forwardQueue: test.out.queue

# Define forwarders
listeners:
  topic-dump:
    enabled: true
    listenConnection: activemq
    topic: misv.admin.topic
    listenerClass: org.ibro.jmstool.receiver.DumpJmsReceiver

defaultConnection: local
```

## Tools
### jms-tool
```bash
$ jms-tool --help
Usage [-c <configFile>] [-a <active-connection>] -q <queue-name> [-b [-d] | [-f <file-to-put-in-queue> ...] [-j <jms-filter-expr>] ] [files...]
Options:
-v|--version	 Show version information
-h|--help	 Show this screen
-c	 configuration file in YAML format - default is jmstool.yaml in $HOME|$CUR_DIR|$JMS_TOOL_HOME
-a	 active named configuration from YAML configuration file
-q	 queue name to browse/send. Multiple queue can be specified (each with -f flag) in browse mode
-t	 topic name to browse/send. Multiple topics can be specified (each with -f flag) in browse mode
-b	 browse mode (don't delete/consume messages) - redirect to write to dump file for later loading if needed
-n	 just count message - don't dump them
-d	 delete browsed message after browse
-r	 dump message mode - read messages from dump input file with headers
-j <jms-filter-expr>	 JMS filter expression for browser, e.g. IE_MSG_CD_FEEDBACK = \'CCN_CoD\' or IE_MSG_CD_FEEDBACK = \'CCN_CoA\'
-e <regex-for-content>	 use regexp provided for content filtering'
-f <file>	 File to read from. Multiple files can be the last argument in command
-x	 Send as TextMessage (default is BytesMessage)
IMPORTANT: -j expression shall be the last parameter. Leave space between operator and property and escape apostrophes
```

### jms-shell

JMS-Shell uses Jolokia protocol to connect to admin message broker interface (if `jolokiaBase` is specified in configuration file).
It retrieves queue and topic names for autocompletion in the shell.

```bash
$ jms-shell 
jms-shell> help
jms-shell (jms-tool v3.1.1)
Copyleft (c) by ibro (ubpaxum@gmail.com) under GPLv3.)
Usage: 
	connect <connection> - creates connection to queue (browser and sender)
	disconnect - disconnects (browser and sender)
	browse <queue> [--filter <filter>] [--regex regex] - browse messages in queue
	delete <queue>  [--filter <filter>] [--regex regex] - delete messages in queue 
	count <queue>  [--filter <filter>] [--regex regex] - count messages in queue 
	send <queue> --file <file> [--format text|byte|model] [--topic] - send message to queue/topic from file
	receive <queue> [--topic] - receive message from queue/topic
	list [objectTypePattern]  - list objects in message broker
	exec <mbean> <operation> <args...>  - execute operation on mbean with arguments (i.e exec org.apache.activemq.artemis:broker="broker" deleteAddress(java.lang.String,boolean) addressToDelete true)
	exit|quit - exit application
	
jms-shell> connect local
Sender created
Browser created
local> send misv.admin.topic --topic --file config-reload.json
Message sent to topic: misv.admin.topic
local> receive ncts.admin.topic --topic
Listening for messages on topic  ncts.admin.topic
local> browse ncts.ccn.out.queue --regex 24BG001002003828K9
Number of messages: 0
local> list :broker=
org.apache.activemq.artemis:broker="0.0.0.0"
org.apache.activemq.artemis:broker="0.0.0.0",component=acceptors,name="amqp"
org.apache.activemq.artemis:broker="0.0.0.0",component=acceptors,name="hornetq"
org.apache.activemq.artemis:broker="0.0.0.0",component=acceptors,name="artemis"
org.apache.activemq.artemis:broker="0.0.0.0",component=acceptors,name="stomp"
org.apache.activemq.artemis:broker="0.0.0.0",component=acceptors,name="mqtt"
local> exec org.apache.activemq.artemis:broker="0.0.0.0"  createQueue(java.lang.String,java.lang.String,java.lang.String) testDynQueue testDynQueue ANYCAST
Response{value=null, timestamp=1733221599, status='200', error='null', request=Request{type='exec', mbean='org.apache.activemq.artemis:broker="0.0.0.0"', operation='createQueue(java.lang.String,java.lang.String,java.lang.String)', arguments=[testDynQueue, testDynQueue, ANYCAST]}}
local> list testDynQueue
org.apache.activemq.artemis:address="testDynQueue",broker="0.0.0.0",component=addresses,queue="testDynQueue",routing-type="anycast",subcomponent=queues
org.apache.activemq.artemis:address="testDynQueue",broker="0.0.0.0",component=addresses
local> exec org.apache.activemq.artemis:broker="0.0.0.0"  deleteAddress(java.lang.String,boolean) testDynQueue true
Response{value=null, timestamp=1733221648, status='200', error='null', request=Request{type='exec', mbean='org.apache.activemq.artemis:broker="0.0.0.0"', operation='deleteAddress(java.lang.String,boolean)', arguments=[testDynQueue, true]}}
local> exit
Disconnected from local
```