package org.ibro.jmstool.receiver;


import org.ibro.jmstool.config.ListenerSetting;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import java.util.Hashtable;

/**
 * Basic functionality for JMS receiver listener..
 * @author ibro
 * Date: 7/4/14
 * Time: 2:48 PM
 */
public interface JMSReceiver extends ExceptionListener {

    public static final String PROP_CONNECT_RETRY_DELAY = "jms.receiver.connect.retry.delay";
    public static final String PROP_DEFAULT_MAX_CONNECT_RETRIES = "jms.receiver.connect.max.retry";

    /**
     * Gets the connection string.
     *
     * @return The connection string
     */
    String getConnectionString();

    /**
     * Sets the connection string.
     *
     * @param connectionString The new connection string
     */
    void setConnectionString(String connectionString);

    /**
     * Gets the connection factory.
     *
     * @return The connection factory
     */
    String getConnectionFactory();

    /**
     * Sets the connection factory.
     *
     * @param connectionFactory The new connection factory
     */
    void setConnectionFactory(String connectionFactory);

    /**
     * Gets the queue name.
     *
     * @return The queue name
     */
    String getQueueName();

    String getTopicName();

    void setTopicName(String topicName);

    /**
     * Sets the queue name.
     *
     * @param queueName The new queue name
     */
    void setQueueName(String queueName);



    Hashtable<String, String> getEnv();

    void setEnv(Hashtable<String, String> env);

    ListenerSetting getSetting();

    void setSetting(ListenerSetting setting);

    /**
     * Gets the status.
     *
     * @return The status
     */
    int getStatus();

    /**
     * Shuts down.
     */
    void down();

    /**
     * On message event entry point method.
     *
     * @param msg The message
     */
    void onMessage(Message msg);

    /* (non-Javadoc)
     * @see javax.jms.ExceptionListener#onException(javax.jms.JMSException)
     */
    void onException(JMSException arg0);

    /**
     * Process message.
     *
     * @param message The message
     * @return The result code, where 0 is OK and negative means failure
     */
    int processMessage(Message message);

    /**
     * Sets the retry count.
     *
     * @param retryCount The new retry count
     */
    void setRetryCount(int retryCount);

    /**
     * Gets the retry count.
     *
     * @return The retry count
     */
    int getRetryCount();

    /**
     * Sets the retry delay.
     *
     * @param ms The new retry delay
     */
    void setRetryDelay(int ms);

    /**
     * Gets the retry delay.
     *
     * @return The retry delay
     */
    int getRetryDelay();

    /**
     * Get JNDI name.
     *
     * @param name The object name
     * @return The JNDI name string
     */
    String jndiName(String name);

}

