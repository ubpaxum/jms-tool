package org.ibro.jmstool.receiver;

import org.ibro.jmstool.model.MessageModel;
import org.ibro.jmstool.util.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.jms.Message;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Hashtable;

public class BimisSaveToFileReceiver extends BaseJMSReceiver {
    private final static Logger log = LoggerFactory.getLogger(BimisSaveToFileReceiver.class);
    private static final String FILE_PATTERN = "%s/BS_TO_AM_%s_%s.xml";

    public BimisSaveToFileReceiver() {
    }

    @Override
    public int processMessage(Message msg) {
        int res = -1;
        MessageModel model = null;
        String msgId= null;
        try {
            msgId = msg.getJMSMessageID();
            model = new MessageModel(msg);
            log.info( "Message received: {}", msgId );

            Document xml = XmlUtils.parse( model.getContentAsStream() );
            Node root = xml.getDocumentElement();
            if ( !root.getNodeName().equals("f") ){
                log.warn("Unexpected message type {} received - not bank statement. Skip it!", root.getNodeName() );
                return 0;
            }

            Element fNode = (Element) XmlUtils.selectSingleNode(root,"./h");
            Element rNode = (Element) XmlUtils.selectSingleNode(root, "./r");

            String timestamp = fNode.getAttribute("timestamp");
            String rn = rNode.getAttribute("rn");

            timestamp = timestamp.replace("-","").replace(":","").replace("T"," ");

            String fileName = String.format(FILE_PATTERN,setting.getPath(), rn,timestamp);
            log.info( "write to {}", fileName );

            try (OutputStream out = new BufferedOutputStream( new FileOutputStream( fileName ) )) {
                XmlUtils.write(xml, out, "windows-1251", true);
            }

            res = 0;
        } catch (Exception e) {
            log.error("Error processing message" ,e);
            log.warn("Stalled message {}\n{}", msgId, model.dump());
        }
        return res;
    }
}
