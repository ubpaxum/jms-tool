package org.ibro.jmstool.receiver;


import org.ibro.jmstool.QueueConnectionWrapper;
import org.ibro.jmstool.QueueConstants;
import org.ibro.jmstool.config.ListenerSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.io.PrintStream;
import java.util.Hashtable;


/**
 * Basic implementation of JMSReceiver.
 */
public abstract  class BaseJMSReceiver extends Thread
        implements MessageListener, JMSReceiver {

    /** The logger. */
    private final static Logger log = LoggerFactory.getLogger(BaseJMSReceiver.class);

    /** The Constant buffer length. */
    private static final int BUFLEN = 65536;
    private static final String DEFAULT_CONNECT_RETRY_DELAY = "10000";
    private static final String DEFAULT_MAX_CONNECT_RETRIES = "10";

    /** The queue connection. */
    protected Connection conn = null;

    /** The queue session. */
    protected Session session = null;

    /** Listener settings */
    protected ListenerSetting setting;

    /** The queue receiver. */
    protected MessageConsumer receiver = null;

    /** The status. */
    protected int status = QueueConstants.STATUS_DISCONNECTED;

    /** The environment parameters. */
    protected Hashtable<String, String> env = null;

    /** The connection string. */
    protected String connectionString = null;

    /** The connection factory. */
    protected String connectionFactory = null;

    /** The queue name. */
    private String queueName = null;

    /** The topic name. */
    private String topicName = null;

    /** The retry count. */
    protected int retryCount = QueueConstants.DEFAULT_MAX_CONNECT_RETRIES;

    /** The retry delay. */
    protected int retryDelay = QueueConstants.DEFAULT_CONNECT_RETRY_DELAY;

    /** The lock. */
    protected Object lock = new Object();

    /** The buffer. */
    protected byte[] buf = new byte[BUFLEN];

    /** The fail count. */
    protected int failCount = 0;

    protected PrintStream out;

    public BaseJMSReceiver() {
        this.out = System.out;
    }

    /**
     * Instantiates a new base JMS receiver.
     *
     * @param connectionString the connection string
     * @param connectionFactory the connection factory
     * @param queueName the queue name
     * @param env the environment parameters
     */
    public BaseJMSReceiver(String connectionString, String connectionFactory, String queueName, String topicName, Hashtable<String, String> env) {
        this();
        this.connectionString = connectionString;
        this.connectionFactory = connectionFactory;
        this.queueName = queueName;
        this.topicName = topicName;
        this.env = env;

        log.info("connectionString = \""+connectionString+"\"");
        log.info("connectionFactory = \""+connectionFactory+"\"");
        log.info("queueName = \"{}\", topicName: \"{}\"",queueName,topicName);
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#getConnectionString()
     */
    @Override
    public String getConnectionString() {
        return connectionString;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#setConnectionString(java.lang.String)
     */
    @Override
    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#getFactory()
     */
    @Override
    public String getConnectionFactory() {
        return connectionFactory;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#setFactory(java.lang.String)
     */
    @Override
    public void setConnectionFactory(String connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#getQueueName()
     */
    @Override
    public String getQueueName() {
        return queueName;
    }

    @Override
    public String getTopicName() {
        return topicName;
    }

    @Override
    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    /* (non-Javadoc)
         * @see org.ccn2.common.queue.JMSReceiver#setQueueName(java.lang.String)
         */
    @Override
    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    @Override
    public Hashtable<String, String> getEnv() {
        return env;
    }

    @Override
    public void setEnv(Hashtable<String, String> env) {
        this.env = env;
    }

    @Override
    public ListenerSetting getSetting() {
        return setting;
    }

    @Override
    public void setSetting(ListenerSetting setting) {
        this.setting = setting;
    }

    /* (non-Javadoc)
             * @see java.lang.Thread#run()
             */
    public void run() {
        try {
            while (true) {
                if ( !init() ){
                    down();
                    break;
                }
                synchronized (lock) {
                    lock.wait();
                }
                close();
                if (status == QueueConstants.STATUS_DOWN) break;
                sleep(retryDelay);
            }
            down();
        } catch (InterruptedException e){
            down();
            log.error("Message listener interrupted");
        }
    }

    /**
     * Initializes the base JMS receiver.
     * It tries to connect to the queue and set itself as a listener.
     * It may retry to connect several times based on the configuration.
     *
     * @return true, if connected successfully
     */
    protected boolean init() {
        boolean connected = false;
        status = QueueConstants.STATUS_DISCONNECTED;
        QueueConnectionWrapper connectionWrapper = new QueueConnectionWrapper(env,connectionFactory);
        while ( (failCount < retryCount) && (status == QueueConstants.STATUS_DISCONNECTED || status == QueueConstants.STATUS_DOWN) ) {
            try {
                conn = connectionWrapper.createConnection();
                //session = conn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
                session = conn.createSession(false, Session.CLIENT_ACKNOWLEDGE);
                receiver = session.createConsumer( queueName!=null?session.createQueue(queueName):session.createTopic(topicName));
                receiver.setMessageListener(this);
                conn.setExceptionListener(this);
                conn.start();
                log.info("Listening for messages on queue "+queueName+" on "+connectionFactory);

                status = QueueConstants.STATUS_CONNECTED;
                return true;
            } catch (Exception e) {
                log.error( String.format("Can not connect to %s queue/topic: %s/%s",connectionFactory,queueName,topicName),e);
                failCount ++;
            }
            close();
            try {
                log.debug(String.format("failCount: %s sleep for %s ms",failCount, retryDelay) );
                sleep(retryDelay);
            } catch (InterruptedException e) {
                log.info("Sleep interrupted - exiting.");
                break;
            }
        }
        if (failCount >= retryCount){
            log.error(String.format("Unable to connect to %s message queue %s after %s attempts. Giving up!",connectionFactory, queueName, failCount ) );
        }
        return connected;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#getStatus()
     */
    @Override
    public int getStatus() {
        return status;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#down()
     */
    @Override
    public void down() {
        log.debug("Shutting down JMS receiver.");
        close();
        synchronized (lock) {
            status = QueueConstants.STATUS_DOWN;
            lock.notifyAll();
        }
        log.debug("JMS receiver is down.");
    }

    /**
     * Close receiver, session and connection.
     */
    protected void close() {
        synchronized (lock) {
            status = QueueConstants.STATUS_DISCONNECTED;
            lock.notifyAll();
            receiver=closeClosable(receiver);
            session=closeClosable(session);
            conn=closeClosable(conn);
        }
    }

    /* (non-Javadoc)
     * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
     */
    @Override
    public void onMessage(Message msg) {
        String msgId = null;
        long startTime = System.currentTimeMillis();

        try {
            msgId = msg.getJMSMessageID();
            log.info( String.format("Processing message JMS ID: %s...",msgId) );
            int res = processMessage(msg);

            /* Do not acknowledge - this shall be consumed by CCN processes. Only send triggering message to initiation queue. */
            if ( res == 0) msg.acknowledge();
            else close();
        } catch (JMSException e) {
            log.error("onMessage", e);
            close();
        } catch (Exception e){
            log.error("Unexpected exception processing message", e);
            close();
        }
        long stopTime = System.currentTimeMillis();
        long duration = stopTime - startTime;
        log.info(String.format("onMessage() took %d ms msgID: %s ", duration, msgId) );
    }


    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#processMessage(javax.jms.Message)
     */
    public abstract int processMessage(Message msg);

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#onException(javax.jms.JMSException)
     */
    @Override
    public void onException(JMSException arg0) {
        log.error(String.format("Unexpected exception for connection: %s queue: %s",connectionFactory, queueName), arg0);
        synchronized (lock) {
            status = QueueConstants.STATUS_DISCONNECTED;
            lock.notifyAll();
        }
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#getRetryCount()
     */
    public int getRetryCount() {
        return retryCount;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#setRetryCount(int)
     */
    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#getRetryDelay()
     */
    public int getRetryDelay() {
        return retryDelay;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#setRetryDelay(int)
     */
    public void setRetryDelay(int retryDelay) {
        this.retryDelay = retryDelay;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSReceiver#jndiName(java.lang.String)
     */
    public String jndiName(String name){
        return name;
    }

    @Override
    protected void finalize() throws Throwable {
        down();
        super.finalize();
    }

    protected <T extends AutoCloseable> T closeClosable(T closeable){
        try{
            closeable.close();
        } catch (Exception e) {
            log.warn("Cannot close {}. Ignore it.",closeable);
        }
        return null;
    }

    public PrintStream getOut() {
        return out;
    }

    public void setOut(PrintStream out) {
        this.out = out;
    }
}
