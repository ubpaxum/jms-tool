package org.ibro.jmstool.receiver;

import org.ibro.jmstool.model.MessageModel;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.Hashtable;

public class DumpJmsReceiver extends BaseJMSReceiver {

    public DumpJmsReceiver() {
    }

    public DumpJmsReceiver(String connectionString, String connectionFactory, String queueName, String topicName, Hashtable<String, String> env) {
        super(connectionString, connectionFactory, queueName, topicName, env);
    }

    @Override
    public int processMessage(Message msg) {
        try {
            MessageModel model = new MessageModel(msg);
            out.println( model.dump() );
        } catch (JMSException e) {
            return -1;
        }
        return 0;
    }
}
