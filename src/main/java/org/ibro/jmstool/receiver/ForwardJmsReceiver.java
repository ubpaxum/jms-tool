package org.ibro.jmstool.receiver;

import org.ibro.jmstool.model.MessageModel;
import org.ibro.jmstool.sender.BaseJMSSender;
import org.ibro.jmstool.sender.MessageSenderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.Hashtable;

public class ForwardJmsReceiver extends BaseJMSReceiver {
    private final static Logger log = LoggerFactory.getLogger(BaseJMSReceiver.class);

    private BaseJMSSender sender;

    public ForwardJmsReceiver(String connectionString, String connectionFactory, String queueName, Hashtable<String, String> env) {
        super(connectionString, connectionFactory, queueName, null, env);
    }

    public BaseJMSSender getSender() {
        return sender;
    }

    public void setSender(BaseJMSSender sender) {
        this.sender = sender;
    }

    @Override
    public int processMessage(Message msg) {
        int res = -1;
        MessageModel model = null;
        try {
            sender.connect();
            model = new MessageModel(msg);
            sender.send( model );
            sender.disconnect();
            res = 0;
        } catch (JMSException | MessageSenderException e) {
            log.error("Error forwarding message: {}",model.dump());
        }
        return res;
    }
}
