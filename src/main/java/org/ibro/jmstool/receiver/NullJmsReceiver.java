package org.ibro.jmstool.receiver;

import javax.jms.Message;
import java.util.Hashtable;

public class NullJmsReceiver extends BaseJMSReceiver {

    public NullJmsReceiver(String connectionString, String connectionFactory, String queueName, Hashtable<String, String> env) {
        super(connectionString, connectionFactory, queueName, null, env);
    }

    @Override
    public int processMessage(Message msg) {
        return 0;
    }
}
