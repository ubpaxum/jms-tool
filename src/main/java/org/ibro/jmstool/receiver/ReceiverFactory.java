package org.ibro.jmstool.receiver;

import org.ibro.jmstool.config.JmsConnectionSetting;
import org.ibro.jmstool.config.ListenerSetting;
import org.ibro.jmstool.sender.BaseFactory;

public class ReceiverFactory extends BaseFactory {

    public static JMSReceiver createBrowser(JmsConnectionSetting setting, String queueName){
        return new NullJmsReceiver("", setting.getFactory(), queueName, createEnv(setting) );
    }

    public static ForwardJmsReceiver createForwarder(JmsConnectionSetting setting, String queueName){
        ForwardJmsReceiver fw =  new ForwardJmsReceiver("", setting.getFactory(), queueName, createEnv(setting));
        fw.setRetryCount(setting.getRetryCount());
        fw.setRetryDelay(setting.getRetryDelay());
        return fw;
    }

    public static BaseJMSReceiver createListener(JmsConnectionSetting setting, ListenerSetting ls){
        BaseJMSReceiver receiver = createInstance( ls.getListenerClass());
        receiver.setConnectionString("");
        receiver.setConnectionFactory(setting.getFactory());
        receiver.setQueueName(ls.getListenQueue());
        receiver.setTopicName(ls.getTopic());
        receiver.setEnv( createEnv(setting) );
        receiver.setRetryCount(setting.getRetryCount());
        receiver.setRetryDelay(setting.getRetryDelay());
        receiver.setSetting( ls );
        return  receiver;
    }

    private static BaseJMSReceiver createInstance(String clazz) throws RuntimeException{
        try {
            return (BaseJMSReceiver) Class.forName(clazz).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot create listener "+clazz);
        }
    }
}
