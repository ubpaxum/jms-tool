package org.ibro.jmstool.transform;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 1/24/15
 * Time: 11:44 AM
 * Formats date as YearMonthDate
 */
public class ShortDateXmlFormatter {

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    public ShortDateXmlFormatter() {
    }

    public String toXml(Object val){
        if ( val == null) return null;
        return dateFormat.format( (Date) val );
    }

    public Object fromXml(String val) throws ParseException {
        if ( val == null) return null;
        return dateFormat.parse(val);
   }
}
