package org.ibro.jmstool.transform;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author blago
 * 
 */
public class ExtraShortDateXmlFormatter{

    private static final DateFormat dateFormat = new SimpleDateFormat("YYMMdd");

    public ExtraShortDateXmlFormatter() {
    }

    public String toXml(Object val){
        if ( val == null) return null;
        return dateFormat.format( (Date) val );
    }

    public Object fromXml(String val) throws ParseException {
        if ( val == null) return null;
        return dateFormat.parse(val);
    }
}
