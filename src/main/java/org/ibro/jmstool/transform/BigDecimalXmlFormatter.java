package org.ibro.jmstool.transform;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;

/**
 * 
 * @author blago
 * 
 */
public class BigDecimalXmlFormatter {

    private static final DecimalFormat decimalFormat = new DecimalFormat("0.#####");

    public BigDecimalXmlFormatter() {
    }

    public String toXml(Object val){
        if ( val == null) return null;
        return decimalFormat.format( (BigDecimal) val );
    }

    public Object fromXml(String val) throws ParseException {
        if ( val == null) return null;
        return decimalFormat.parse(val);
    }
}
