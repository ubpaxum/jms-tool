package org.ibro.jmstool.transform;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 1/24/15
 * Time: 11:44 AM
 * Format time as HHmm (hour:minute)
 */
public class ShortTimeXmlFormatter{

    private static final DateFormat dateFormat = new SimpleDateFormat("HHmm");

    public ShortTimeXmlFormatter() {
    }

    public String toXml(Object val){
        if ( val == null) return null;
        return dateFormat.format( (Date) val );
    }

    public Object fromXml(String val) throws ParseException {
        if ( val == null) return null;
        return dateFormat.parse(val);
    }
}
