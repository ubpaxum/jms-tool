package org.ibro.jmstool;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.hornetq.api.core.DiscoveryGroupConfiguration;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.jms.HornetQJMSClient;
import org.hornetq.api.jms.JMSFactoryType;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.jms.client.HornetQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 6/12/15
 * Time: 11:42 AM
 * Short description of purpose.
 */
public class QueueConnectionWrapper {
    private Hashtable<String,String> env;
    private String connectionFactory;
    private String connType = null;
    private static Logger log = LoggerFactory.getLogger(QueueConnectionWrapper.class);
    private String url ;
    private String username;
    private String password;

    public QueueConnectionWrapper(Hashtable<String, String> env, String connectionFactory) {
        this.env = new Hashtable<String, String>();

        // copy original map to class specific - some items may be removed
        this.env.putAll(env);
        // remove from environment - this is custom property - non-standart and just for the sake of connection wrapper
        connType = this.env.remove(QueueConstants.PROP_JMS_TYPE);
        
        this.connectionFactory = connectionFactory;
        init();
    }

    private void init(){
        this.url = env.get(Context.PROVIDER_URL);
        this.username = env.get(Context.SECURITY_PRINCIPAL);
        this.password = env.get(Context.SECURITY_CREDENTIALS);
    }

    public QueueConnection createConnection() throws JMSException, NamingException {
        QueueConnection conn = null;
        if ( connType == null || connType.equals("context") ){
            conn = createConnectionFromContext();
        } else if ( connType.equals("hornetq") ){
            conn = createHornetConnection();
        } else if ( connType.equals("hornetq-ha") ){
            conn = createHornetHAConnection();
        } else if ( connType.equals("activemq") ){
            conn = createActiveMQConnection();
        } else{
            throw new IllegalArgumentException("Connection type "+connType+" is not supported!");
        }
        log.info(String.format("Queue connection: %s", conn.getMetaData()));
        if ( conn == null )
            throw new JMSException("Connection is null. Check your configuration or server status.");
        return conn;
    }

    private QueueConnection createHornetConnection() throws JMSException {
        log.info(String.format("Provider url: %s", url));
        if ( url == null  )
            throw new IllegalArgumentException("URL for hornet server must be specified");

        int index = url.indexOf(':');
        if ( index < 0)
            throw new IllegalArgumentException("URL for hornet server must be in format hostname:port");

        String aHostname = url.substring(0, index );
        String aPort = url.substring(index+1);

        Map newEnv = new HashMap();
        newEnv.put("host", aHostname);
        try {
            newEnv.put("port", Integer.valueOf(aPort));
        }
        catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid port: "+ aPort);
        }

        TransportConfiguration initialServer =
                new TransportConfiguration(NettyConnectorFactory.class.getName(), newEnv);
        System.out.println(initialServer);
//        HornetQConnectionFactory factory =
//                HornetQJMSClient
//                        .createConnectionFactoryWithoutHA(JMSFactoryType.QUEUE_CF,
//                                new TransportConfiguration[]{initialServer});
        HornetQConnectionFactory factory =
                HornetQJMSClient
                        .createConnectionFactoryWithHA(JMSFactoryType.CF,
                                new TransportConfiguration[]{initialServer});
        return factory.createQueueConnection();
    }

    private QueueConnection createActiveMQConnection() throws JMSException {
        log.info(String.format("Provider url: %s", url));
        if ( url == null  )
            throw new IllegalArgumentException("URL for hornet server must be specified");

        log.debug("Try connecting to "+url);
        ActiveMQConnectionFactory factory = username==null?
                 new ActiveMQConnectionFactory(url)
                :new ActiveMQConnectionFactory(username,password,url);
        return factory.createQueueConnection();
    }

    private QueueConnection createHornetHAConnection() throws JMSException {
        log.info(String.format("Provider url: %s", url));
        if ( url == null  )
            throw new IllegalArgumentException("URL for hornet server must be specified");

        int index = url.indexOf(':');
        if ( index < 0)
            throw new IllegalArgumentException("URL for hornet server must be in format hostname:port");

        String groupAddress = url.substring(0, index );
        int groupPort = Integer.parseInt(url.substring(index + 1));

        HornetQConnectionFactory factory = HornetQJMSClient.createConnectionFactoryWithHA(new DiscoveryGroupConfiguration(groupAddress, groupPort), JMSFactoryType.CF);
        log.info("ConnectionFactory.isHA: "+factory.isHA());
        return factory.createQueueConnection();
    }

    private QueueConnection createConnectionFromContext() throws NamingException, JMSException {
        log.info(String.format("Trying to connect to %s ...", connectionFactory));
        InitialContext ctx = new InitialContext(env);
        QueueConnectionFactory connFactory = (QueueConnectionFactory) ctx.lookup( connectionFactory );
        log.info(String.format("Connection factory to %s done.",connectionFactory));
        ctx.close();
        return connFactory.createQueueConnection();
    }
}
