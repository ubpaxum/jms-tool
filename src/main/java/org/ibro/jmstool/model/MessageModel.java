package org.ibro.jmstool.model;

import org.ibro.jmstool.util.StreamUtils;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 8/10/15
 * Time: 6:18 PM
 * Short description of purpose.
 */
public class MessageModel {
    private String messageType;
    private Map<String,Object> headers;
    private StringBuilder stringBuilder;
    private byte[]   bytes;

    public MessageModel() {
        headers = new HashMap<String, Object>();
        stringBuilder = new StringBuilder();
    }

    public MessageModel(Message msg) throws JMSException {
        headers = new HashMap<String, Object>();
        this.messageType = msg.getClass().getSimpleName();
        if ( msg instanceof TextMessage ){
            stringBuilder = new StringBuilder( ((TextMessage) msg).getText() );
        } else if ( msg instanceof BytesMessage ){
            this.bytes = StreamUtils.toByteArray( (BytesMessage) msg );
        } else {
            stringBuilder = new StringBuilder();
        }
        Enumeration<String> headers = msg.getPropertyNames();
        while ( headers.hasMoreElements() ){
            String propName = headers.nextElement();
            addHeader( propName, msg.getObjectProperty(propName) );
        }

    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getContent() {
        return stringBuilder!=null?stringBuilder.toString(): new String( bytes ) ;
    }

    public void setContent(byte[] bytes) {
        stringBuilder = null;
        this.bytes = bytes;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    public void addHeader(String header, Object val){
        headers.put(header,val);
    }

    public void addLine(String line){
        if ( line != null){
            if ( stringBuilder.length() > 0 )  stringBuilder.append("\n");
            stringBuilder.append(line);
        }
    }

    public InputStream getContentAsStream(){
        return new ByteArrayInputStream( bytes==null? stringBuilder.toString().getBytes(): bytes );
    }

    public String dump(){
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("\n==> BEGIN HEADER ==>\n"));
        builder.append(String.format("JMSType: %s\n", getMessageType()));
        for ( Map.Entry<String,Object> entry: getHeaders().entrySet() ){
            builder.append(String.format("%s: %s\n", entry.getKey(), entry.getValue()) );
        }
        builder.append(String.format("==> END HEADER ==>\n"));
        builder.append(String.format("==> BEGIN MESSAGE ==>\n") );
        builder.append(String.format("%s\n", getContent()) );
        builder.append(String.format("==> END MESSAGE ==>\n") );
        return builder.toString();
    }
}