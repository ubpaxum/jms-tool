package org.ibro.jmstool;

import org.ibro.jmstool.config.ForwardSetting;
import org.ibro.jmstool.config.JmsConnectionSetting;
import org.ibro.jmstool.config.JmsToolConfig;
import org.ibro.jmstool.receiver.ForwardJmsReceiver;
import org.ibro.jmstool.receiver.ReceiverFactory;
import org.ibro.jmstool.sender.BaseJMSSender;
import org.ibro.jmstool.sender.SenderFactory;
import org.ibro.jmstool.starter.ShutdownHook;
import org.ibro.jmstool.starter.Starter;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 6/9/15
 * Time: 5:15 PM
 * Short description of purpose.
 */
public class Forwarder extends MainBase{
    private static final String USAGE =
            "Usage [-c <configFile>] [--dry-run] \n" +
                    "\tOptions:\n" +
                    "\t-c\t configuration file in YAML format - default is jmstool.yaml in $HOME|$CUR_DIR|$JMS_TOOL_HOME\n" +
                    "\t--dry-run\t Do not start forwarders - just print what shall be done\n";

    public static void main(String[] args){
        initLogger();
        String configFile = null;
        boolean isConfig = false;
        boolean isDryRun = false;
        for (String arg: args ){
            if ( arg.equals("-c") ){
                isConfig = true;
                continue;
            }
            if ( arg.equals("--dry-run") ){
                isDryRun = true;
                continue;
            }
            if ( isConfig ){
                configFile = arg;
                isConfig = false;
                continue;
            }
            dieWithInvalidArgs("unknown argument",USAGE);
        }


        try {
            JmsToolConfig config = null;
            File fConfig  = getConfigFile(configFile);
            if ( fConfig == null )
                dieWithInvalidArgs("Configuration file not specified or cannot be read", USAGE);

            try (InputStream is = new FileInputStream( fConfig )){
                config = new Yaml().loadAs(is,JmsToolConfig.class);
            }


            if ( config.getForwarders() == null )
                dieWithInvalidArgs("No forwarders specified "+configFile, USAGE);

            Starter starter = new Starter();
            for ( String forwarderName: config.getForwarders().keySet() ){
                ForwardSetting fs = config.getForwarders().get(forwarderName);
                log.info("Inspecting forwarder: {}: {} ", forwarderName, fs);
                if ( fs.isEnabled() ){
                    if ( isDryRun ){
                        System.out.printf("Forwarders: %s: %s\n",forwarderName,fs);
                    } else {
                        JmsConnectionSetting jr = config.getConnectionSetting(  fs.getListenConnection() );
                        JmsConnectionSetting js = config.getConnectionSetting(  fs.getForwardConnection() );
                        ForwardJmsReceiver forwarder = ReceiverFactory.createForwarder(jr, fs.getListenQueue() );
                        BaseJMSSender sender = SenderFactory.createSender( js, fs.getForwardQueue() );
                        forwarder.setSender(sender);
                        starter.addListener(forwarderName,forwarder);
                    }
                }
            }
            if ( !isDryRun ) {
                Runtime.getRuntime().addShutdownHook(new ShutdownHook(starter));
                starter.start();
            }
        } catch (Exception e) {
            log.error("Unhandled exception", e);
            System.exit(-2);
        } finally {
        }
    }

}
