package org.ibro.jmstool.jolokia;


/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2024-10-14
 */
public class IntResponse extends BaseResponse {
    private Integer value;


    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Response{" +
                "value=" + value +
                super.toString() +
                '}';
    }
}
