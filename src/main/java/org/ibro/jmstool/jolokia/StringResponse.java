package org.ibro.jmstool.jolokia;


/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2024-10-14
 */
public class StringResponse extends BaseResponse {
    private String value;


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Response{" +
                "value=" + value +
                super.toString() +
                '}';
    }
}
