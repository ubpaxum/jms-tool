package org.ibro.jmstool.jolokia;


import java.io.Serializable;

/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2024-10-14
 */
public abstract class BaseResponse implements Serializable {
    private Request request;
    private Long timestamp;
    private Integer status;
    private String error;
    private String error_type;

    public BaseResponse() {
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError_type() {
        return error_type;
    }

    public void setError_type(String error_type) {
        this.error_type = error_type;
    }

    public abstract Object getValue();

    @Override
    public String toString() {
        return  ", timestamp=" + timestamp +
                ", status=" + status +
                (error==null?"":", error='" + error + '\'' );
    }
}
