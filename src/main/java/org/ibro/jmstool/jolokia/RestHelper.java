package org.ibro.jmstool.jolokia;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import jakarta.ws.rs.core.MediaType;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;


public class RestHelper {
    private static final Logger log = LoggerFactory.getLogger(RestHelper.class);

    private String baseUrl;
    private String user;
    private String password;
    private String origin;


    public RestHelper(String baseUrl, String user, String password, String origin) {
        this.baseUrl = baseUrl;
        this.user = user;
        this.password = password;
        this.origin = origin;
    }

    public <T> T invokeGet(String uri, Class<T> targetClass) {
        try{
            String authUrl = baseUrl + uri;
            WebClient client = WebClient.create(authUrl, user, password, null);
            String response = client
                    .accept(MediaType.APPLICATION_JSON)
                    .header("Origin", origin)
                    .type(MediaType.APPLICATION_JSON)
                    .get(String.class);
            log.debug("REST response: {}", response);
            T result = new Gson().fromJson(response,targetClass);
            return result;
        } catch (Throwable e) {
            log.error("Cannot call {}{}: {}", baseUrl,uri, e);
            return null;
        }
    }

    public <T extends BaseResponse> T invokePost(String uri, Object body, Class<T> targetClass) {
        try{
            String authUrl = baseUrl + uri;
            WebClient client = WebClient.create(authUrl, user, password, null);
            log.debug("REST request: {}", body);
            String response = client
                    .accept(MediaType.APPLICATION_JSON)
                    .header("Origin", origin)
                    .type(MediaType.APPLICATION_JSON)
                    .post(new Gson().toJson(body),String.class);
            log.debug("REST response: {}", response);
            return parseResponse(response, targetClass);
        } catch (Throwable e) {
            log.error("Cannot call {}{}: {}", baseUrl,uri, e);
            return null;
        }
    }

    private  <T extends BaseResponse> T parseResponse(String response, Class<T> targetClass){
        if ( targetClass != BaseResponse.class ){
            return new Gson().fromJson(response,targetClass);
        }
        List<Class<? extends BaseResponse>> classes = Arrays.asList(StringArrayResponse.class, StringResponse.class, IntResponse.class, MapResponse.class);
        for ( Class clazz: classes ){
            try{
                return (T) new Gson().fromJson(response,clazz);
            } catch (JsonSyntaxException e){
                log.warn("Did not succeed with {}. Try next one", clazz);
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "RestHelper{" +
                "baseUrl='" + baseUrl + '\'' +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                ", origin='" + origin + '\'' +
                '}';
    }
}
