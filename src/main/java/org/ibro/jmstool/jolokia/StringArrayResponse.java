package org.ibro.jmstool.jolokia;


import java.util.List;

/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2024-10-14
 */
public class StringArrayResponse extends BaseResponse {
    private List<String> value;


    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Response{" +
                "value=" + value +
                super.toString() +
                '}';
    }
}
