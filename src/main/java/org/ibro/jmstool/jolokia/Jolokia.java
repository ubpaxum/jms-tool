package org.ibro.jmstool.jolokia;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2024-10-14
 */
public class Jolokia {
    private String user;
    private String password;
    private String origin;
    private String baseUrl;
    private String pattern;
    private List<String> brokerInfo;
    private List<String> addresses;

    public Jolokia(String user, String password, String origin, String baseUrl, String pattern) {
        this.user = user;
        this.password = password;
        this.origin = origin;
        this.baseUrl = baseUrl;
        this.pattern = pattern;
        this.addresses = new ArrayList<>();
        this.brokerInfo = new ArrayList<>();
    }

    public List<String> getAddresses() {
        return this.addresses;
    }

    public List<String> getBrokerInfo() {
        return brokerInfo;
    }

    public void extractAddresses(){
        List<String> info = list(Pattern.compile(".*"));
        Pattern pattern = Pattern.compile(this.pattern);
        for(String address: info){
            brokerInfo.add(address);
            Matcher m = pattern.matcher(address);
            if ( m.find() ){
                if ( !addresses.contains(m.group(1)) ) addresses.add( m.group(1) );
            }
        }
    }

    public List<String> list(Pattern pattern){
        if ( baseUrl == null ) return Collections.EMPTY_LIST;
        List<String> result = new ArrayList<>();
        RestHelper rest = new RestHelper(baseUrl, user, password, origin);
        StringArrayResponse response = rest.invokeGet("/search/*:*", StringArrayResponse.class);
        if (response != null && response.getValue()!=null){
            for (String val: response.getValue()){
                if ( pattern.matcher(val).find() ){
                    result.add( val );
                }
            }
        }
        return result;
    }

    public String exec(String mbean, String operation, List<String> arguments){
        if ( baseUrl == null ) return null;
        RestHelper rest = new RestHelper(baseUrl, user, password, origin);
        Request request = createExecRequest(mbean, operation, arguments);
        BaseResponse response = rest.invokePost("/exec", request, BaseResponse.class);
        return response==null?"No response": (
                  response.getStatus()==200?(response.getValue()==null?"":response.getValue().toString()):response.toString()
                );
    }

    private Request createExecRequest(String mbean, String operation, List<String> arguments){
        return new Request("exec", mbean, operation, arguments);
    }

}
