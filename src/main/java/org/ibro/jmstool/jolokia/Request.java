package org.ibro.jmstool.jolokia;

import java.util.List;

/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2024-12-02
 */
public class Request {
    private String type;
    private String mbean;
    private String operation;
    private List<String> arguments;

    public Request() {
    }

    public Request(String type, String mbean, String operation, List<String> arguments) {
        this.type = type;
        this.mbean = mbean;
        this.operation = operation;
        this.arguments = arguments;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMbean() {
        return mbean;
    }

    public void setMbean(String mbean) {
        this.mbean = mbean;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public List<String> getArguments() {
        return arguments;
    }

    public void setArguments(List<String> arguments) {
        this.arguments = arguments;
    }

    @Override
    public String toString() {
        return "Request{" +
                "type='" + type + '\'' +
                ", mbean='" + mbean + '\'' +
                ", operation='" + operation + '\'' +
                ", arguments=" + arguments +
                '}';
    }
}
