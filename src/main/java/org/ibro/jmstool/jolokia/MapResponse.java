package org.ibro.jmstool.jolokia;


import java.util.Map;

/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2024-10-14
 */
public class MapResponse extends BaseResponse {
    private Map value;


    public Map getValue() {
        return value;
    }

    public void setValue(Map value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Response{" +
                "value=" + value +
                super.toString() +
                '}';
    }
}
