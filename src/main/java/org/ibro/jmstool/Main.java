package org.ibro.jmstool;

import org.ibro.jmstool.browser.BaseJMSBrowser;
import org.ibro.jmstool.browser.BrowserFactory;
import org.ibro.jmstool.config.JmsConnectionSetting;
import org.ibro.jmstool.config.JmsToolConfig;
import org.ibro.jmstool.model.MessageModel;
import org.ibro.jmstool.sender.BaseJMSSender;
import org.ibro.jmstool.sender.JMSSender;
import org.ibro.jmstool.sender.SenderFactory;
import org.ibro.jmstool.util.MessageFormatReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 6/9/15
 * Time: 5:15 PM
 * Short description of purpose.
 */
public class Main extends MainBase{
    private static final String USAGE =
            "Usage:\n" +
                    "jms-tool [-c <configFile>] [-a <active-connection>] -q <queue-name> [-b [-d] | [-f <file-to-put-in-queue> ...] [-j <jms-filter-expr>] ] [files...]\n" +
                    "\tOptions:\n" +
                    "\t-v|--version\t Show version information\n" +
                    "\t-h|--help\t Show this screen\n" +
                    "\t-c\t configuration file in YAML format - default is jmstool.yaml in $HOME|$CUR_DIR|$JMS_TOOL_HOME\n" +
                    "\t-a\t active named configuration from YAML configuration file\n" +
                    "\t-q\t queue name to browse/send. Multiple queue can be specified (each with -f flag) in browse mode\n" +
                    "\t-t\t topic name to browse/send. Multiple topics can be specified (each with -f flag) in browse mode\n" +
                    "\t-b\t browse mode (don't delete/consume messages) - redirect to write to dump file for later loading if needed\n" +
                    "\t-n\t just count message - don't dump them\n" +
                    "\t-d\t delete browsed message after browse\n" +
                    "\t-r\t dump message mode - read messages from dump input file with headers\n" +
                    "\t-j <jms-filter-expr>\t JMS filter expression for browser, e.g. IE_MSG_CD_FEEDBACK = \\'CCN_CoD\\' or IE_MSG_CD_FEEDBACK = \\'CCN_CoA\\'\n" +
                    "\t-e <regex-for-content>\t use regexp provided for content filtering'\n" +
                    "\t-f <file>\t File to read from. Multiple files can be the last argument in command\n" +
                    "\t-x\t Send as TextMessage (default is BytesMessage)\n" +
                    "\tIMPORTANT: -j expression shall be the last parameter. Leave space between operator and property and escape apostrophes \n";


    public static void main(String[] args){
        initLogger();
        String configFile = null;
        List<String> messageFiles = new ArrayList<>();
        List<String> queues = new ArrayList<>();
        List<String> topics = new ArrayList<>();
        String app = null;
        boolean isConfig = false;
        boolean isFile = false;
        boolean isQueue = false;
        boolean isTopic = false;
        boolean isActiveConfig = false;
        boolean isRaw = false;
        boolean isBrowse = false;
        boolean isCount = false;
        boolean isDelete = false;
        boolean isJmsFilter = false;
        boolean isTextMessage = false;
        boolean isRegexp = false;
        String  jmsFilter = null;
        String  regexFilter = null;
        for (String arg: args ){
            if ( arg.equals("-c") ){
                isConfig = true;
                continue;
            }
            if ( arg.equals("-b") ) {
                isBrowse = true;
                continue;
            }
            if ( arg.equals("-n") ) {
                isCount = true;
                continue;
            }
            if ( arg.equals("-j") ) {
                isJmsFilter = true;
                continue;
            }
            if ( arg.equals("-d") ) {
                isDelete = true;
                continue;
            }
            if ( arg.equals("-f") ) {
                isFile = true;
                continue;
            }
            if ( arg.equals("-q") ) {
                isQueue = true;
                continue;
            }
            if ( arg.equals("-t") ) {
                isTopic = true;
                continue;
            }
            if ( arg.equals("-r") ) {
                isRaw = true;
                continue;
            }
            if ( arg.equals("-e") ) {
                isRegexp = true;
                continue;
            }
            if ( arg.equals("-x") ) {
                isTextMessage = true;
                continue;
            }
            if ( arg.equals("-a") ) {
                isActiveConfig = true;
                continue;
            }
            if ( arg.equals("-h") || arg.equals("--help")){
                dieWithInvalidArgs("", USAGE);
            }

            if ( arg.equals("-v") || arg.equals("--version")){
                showVersion("jms-tool");
            }

            if ( isConfig ){
                configFile = arg;
                isConfig = false;
                continue;
            }
            if ( isFile ) {
                messageFiles.add(arg);
                isFile = false;
                continue;
            }
            if ( isQueue ) {
                queues.add(arg);
                isQueue = false;
                continue;
            }
            if ( isTopic ) {
                topics.add(arg);
                isTopic = false;
                continue;
            }
            if ( isActiveConfig ) {
                app = arg;
                isActiveConfig = false;
                continue;
            }
            if ( isJmsFilter ) {
                if ( jmsFilter == null ) jmsFilter = arg;
                else jmsFilter = jmsFilter+" "+arg;
//                isJmsFilter = false;
                continue;
            }
            if ( isRegexp ) {
                if ( regexFilter == null ) regexFilter = arg;
                else regexFilter = regexFilter+" "+arg;
                continue;
            }
            // no matching so far -interpret it as file
            if ( arg.startsWith("-") ) dieWithInvalidArgs("unknown argument", USAGE);
            else messageFiles.add(arg);
        }

        if ( (queues.size()==0 && topics.size()==0)|| !(isBrowse || messageFiles.size()>0) )
            dieWithInvalidArgs("Specify queue(s) name and browse or file/dump load options", USAGE);

        if ( (queues.size()>1 || topics.size()>1) && !isBrowse )
            dieWithInvalidArgs("Only one queue/topic is allowed for send mode.", USAGE);

        try {
            JmsToolConfig config = null;
            File fConfig  = getConfigFile(configFile);
            if ( fConfig == null )
                dieWithInvalidArgs("Configuration file not specified or cannot be read", USAGE);

            try (InputStream is = new FileInputStream( fConfig )){
                config = new Yaml().loadAs(is,JmsToolConfig.class);
            }

            if ( app == null ) app = config.getDefaultConnection();

            if ( app == null )
                dieWithInvalidArgs("Active configuration not specified and not set in configuration file!", USAGE);


            JmsConnectionSetting setting = config.getConnectionSetting(app);
            if ( setting == null )
                dieWithInvalidArgs("Configuration "+app+" not found in "+configFile, USAGE);

            if ( isBrowse ){
                BaseJMSBrowser browser = BrowserFactory.createBrowser(setting, null);
                browser.setFilter(jmsFilter);
                browser.setRegexFilter(regexFilter);
                browser.setDelete(isDelete);
                browser.setCount(isCount);
                for ( String queue: queues ){
                    browser.setQueueName(queue);
                    int messages = browser.browse();
                    System.out.printf("%s: %s\n",queue, messages);
                }
                browser.setQueueName(null);
                for ( String topic: topics ){
                    browser.setBrowseTopicName(topic);
                    int messages = browser.browse();
                    System.out.printf("%s: %s\n",topic, messages);
                }

                browser.disconnect();
            } else if ( messageFiles.size() > 0 ){
                BaseJMSSender sender = null;
                String queueTopic = null;
                if ( queues.size()> 0 ){
                    queueTopic = queues.get(0);
                    sender = SenderFactory.createSender(setting,queueTopic);
                } else {
                    queueTopic = topics.get(0);
                    sender = SenderFactory.createTopicSender(setting,queueTopic);
                }
                sender.setMessageMode( isTextMessage? JMSSender.MessageMode.TEXT: JMSSender.MessageMode.BYTE );
                log.info("About to send message to {} queue/topic: {}",app,queueTopic );
                for ( String messageFile: messageFiles){
                    log.info("Read file {}, raw: {}, mode: {}", messageFile, isRaw, sender.getMessageMode());
                    InputStream is = new FileInputStream(messageFile);
                    try{
                        if ( isRaw ){
                            MessageFormatReader reader = new MessageFormatReader(messageFile);
                            reader.loadMessages();
                            for ( MessageModel model: reader.getMessages() ){
                                sender.send(model);
                            }
                        } else {
                            sender.send(is);
                        }
                    } finally {
                        if ( is != null) is.close();
                    }
                }
                sender.disconnect();
            }

        } catch (Exception e) {
            log.error("Unhandled exception", e);
            System.exit(-2);
        } finally {
        }
    }

}
