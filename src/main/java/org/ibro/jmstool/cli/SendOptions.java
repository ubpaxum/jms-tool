package org.ibro.jmstool.cli;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2024-10-11
 */
public class SendOptions {
    @Parameter(description = "queue name", required = true)
    private String queueName;
    @Parameter(names = {"--file","-f"})
    private String fileName = null;

    @Parameter(names = {"--format","-t"})
    private String format = "text";

    @Parameter(names = "--topic")
    private boolean topic = false;

    public void parse(String[] args) {
        JCommander.newBuilder()
                .addObject(this)
                .build()
                .parse(args);
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public boolean isTopic() {
        return topic;
    }

    public void setTopic(boolean topic) {
        this.topic = topic;
    }
}
