package org.ibro.jmstool.cli;

import org.ibro.jmstool.Shell;
import org.ibro.jmstool.Version;
import org.ibro.jmstool.browser.BaseJMSBrowser;
import org.ibro.jmstool.browser.BrowserFactory;
import org.ibro.jmstool.config.JmsConnectionSetting;
import org.ibro.jmstool.config.ListenerSetting;
import org.ibro.jmstool.jolokia.Jolokia;
import org.ibro.jmstool.model.MessageModel;
import org.ibro.jmstool.receiver.BaseJMSReceiver;
import org.ibro.jmstool.receiver.ReceiverFactory;
import org.ibro.jmstool.sender.BaseJMSSender;
import org.ibro.jmstool.sender.JMSSender;
import org.ibro.jmstool.sender.MessageSenderException;
import org.ibro.jmstool.sender.SenderFactory;
import org.ibro.jmstool.util.MessageFormatReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2024-10-11
 */
public class CommandParser {
    private Logger log = LoggerFactory.getLogger(CommandParser.class);
    private Shell shell;
    private PrintStream out;
    private BaseJMSSender sender;
    private BaseJMSBrowser browser;
    private BaseJMSReceiver receiver;
    private Jolokia jolokia;
    private FileOutputStream fos;
    private String activeConnection;
    private final String USAGE ="jms-shell "+ Version.VERSION+"\n"+
            Version.COPYRIGHT+"\n" +
            "Usage: \n" +
            "\tconnect <connection> - creates connection to queue (browser and sender)\n" +
            "\tdisconnect - disconnects (browser and sender)\n" +
            "\tbrowse <queue> [--filter <filter>] [--regex regex] - browse messages in queue\n" +
            "\tdelete <queue>  [--filter <filter>] [--regex regex] - delete messages in queue \n" +
            "\tcount <queue>  [--filter <filter>] [--regex regex] - count messages in queue \n" +
            "\tsend <queue> --file <file> [--format text|byte|model] [--topic] - send message to queue/topic from file\n" +
            "\treceive <queue> [--topic] - receive message from queue/topic\n" +
            "\tlist [objectTypePattern]  - list objects in message broker\n" +
            "\texec <mbean> <operation> <args...>  - execute operation on mbean with arguments (i.e exec org.apache.activemq.artemis:broker=\"broker\" deleteAddress(java.lang.String,boolean) addressToDelete true)\n" +
            "\texit|quit - exit application\n";

    public CommandParser(Shell shell) {
        this.shell = shell;
        this.out = shell.getOut();
        info("jms-shell %s\n%s\n",Version.VERSION, Version.COPYRIGHT);
    }

    public int process(String line)  {
        int result = 0;
        StringTokenizer tokenizer = new StringTokenizer(line);
        String command = null;
        List<String> params = new ArrayList<>();
        while (tokenizer.hasMoreTokens()){
            String token = tokenizer.nextToken();
            if ( command==null ){ // first is always command
                command = token;
            } else { // parameters
                params.add(token);
            }
        }
        if (command == null ) return 1;
        try{
            if ( "connect".equalsIgnoreCase(command) ){
                if ( params.size() == 1 ){
                    connect(params.get(0));
                } else {
                    error("usage: connect <connection>\n");
                }
            } else if ( "browse".equalsIgnoreCase(command) ) {
                browse(params);
            } else if ( "count".equalsIgnoreCase(command) ) {
                count(params);
            } else if ( "delete".equalsIgnoreCase(command) ) {
                delete(params);
            } else if ( "send".equalsIgnoreCase(command) ) {
                send(params);
            } else if ( "receive".equalsIgnoreCase(command) ) {
                receive(params);
            } else if ( "list".equalsIgnoreCase(command) ) {
                list(params);
            } else if ( "exec".equalsIgnoreCase(command) ) {
                exec(params);
            } else if ( "spool".equalsIgnoreCase(command) ) {
                spool(params);
            } else if ( "disconnect".equalsIgnoreCase(command) ) {
                disconnect();
            } else if ( "quit".equalsIgnoreCase(command) || "exit".equalsIgnoreCase(command) ) {
                disconnect();
                result = -1;
            } else {
                usage();
            }
        } catch (Exception e){
            log.error("Error processing "+line, e);
            error("Error: %s", e.getMessage());
        }
        return result;

    }

    void connect(String connectionName){
        disconnect();
        if (! shell.getConfig().getConnections().keySet().contains(connectionName)){
            error("Connection %s not defined!",connectionName);
            return;
        }
        JmsConnectionSetting settings = shell.getConfig().getConnections().get(connectionName);
        sender = SenderFactory.createSender( settings,  null );
        sender.connect();
        info("Sender created\n");
        browser = BrowserFactory.createBrowser(settings, null);
        browser.connect();
        info("Browser created\n");
        shell.setPrompt(connectionName);
        jolokia = new Jolokia( settings.getUserName(), settings.getUserPassword(), settings.getOrigin(), settings.getJolokiaBase(), settings.getPattern() );
        jolokia.extractAddresses();
        shell.getCompleter().setAutocompleteObjects(jolokia.getAddresses());
        activeConnection = connectionName;
    }

    void browse(List<String> args) throws MessageSenderException {
        BrowseOptions options = new BrowseOptions();
        int res = browse(options, args);
        info("Number of messages: %s\n", res );
    }

    void count(List<String> args) throws MessageSenderException {
        BrowseOptions options = new BrowseOptions();
        options.isCount = true;
        int res = browse(options, args);
        info("Count of messages: %s\n", res );
    }

    void delete(List<String> args) throws MessageSenderException {
        BrowseOptions options = new BrowseOptions();
        options.isCount = true;
        options.isDelete = true;
        int res = browse(options, args);
        info("Deleted messages: %s\n", res );
    }

    int browse(BrowseOptions options, List<String> args) throws MessageSenderException {
        if ( browser == null  ){
            error("Not connected\n");
            return -1;
        }
        if ( !options.parse(args) ) {
            error("Parse failure: %s\n", options.status);
        }

        browser.setQueueName(options.queueName);
        browser.setCount(options.isCount);
        browser.setDelete(options.isDelete);
        browser.setRegexFilter(options.regexFilter);
        browser.setFilter(options.jmsFilter);
        browser.setOut(this.out);
        return browser.browse();
    }

    void send(List<String> args) throws Exception {
        if ( sender == null  ){
            error("Not connected\n");
            return;
        }
        SendOptions options = new SendOptions();
        options.parse( args.toArray(new String[0]) );
        List<MessageModel> messages =  null;
        InputStream content = null;
        if ( "model".equalsIgnoreCase(options.getFormat()) ){
            MessageFormatReader reader = new MessageFormatReader(options.getFileName());
            reader.loadMessages();
            messages = reader.getMessages();
        } else {
            content =  new FileInputStream(options.getFileName());
        }
        if ( options.isTopic() ) sender.setTopicName( options.getQueueName() );
        else sender.setQueueName( options.getQueueName() );
        if ( content!=null ){
            sender.setMessageMode( "byte".equals(options.getFormat())? JMSSender.MessageMode.BYTE: JMSSender.MessageMode.TEXT );
            sender.send( content  );
            info("Message sent to %s: %s\n", options.isTopic()?"topic":"queue", options.getQueueName() );
        } else {
            int count=0;
            for (MessageModel mm: messages){
                sender.send( mm  );
                count++;
            }
            info("%s messages sent to %s: %s\n", count, options.isTopic()?"topic":"queue", options.getQueueName() );
        }
    }

    void receive(List<String> args) throws Exception {
       if (activeConnection == null || args.size()<1){
            error("Not connected.");
            return;
        }
        ReceiveOptions options = new ReceiveOptions();
        options.parse( args.toArray(new String[0]) );
        JmsConnectionSetting settings = shell.getConfig().getConnections().get(activeConnection);
        ListenerSetting ls = new ListenerSetting();
        ls.setEnabled(true);
        if ( options.isTopic() ) ls.setTopic(options.getQueueName());
        else ls.setListenQueue(options.getQueueName());
        ls.setListenerClass("org.ibro.jmstool.receiver.DumpJmsReceiver");
        ls.setListenConnection(activeConnection);
        receiver = ReceiverFactory.createListener( settings, ls );
        receiver.setOut(this.out);
        receiver.start();
        info("Listening for messages on %s  %s\n", options.isTopic()?"topic":"queue", options.getQueueName() );
    }

    void list(List<String> args) throws MessageSenderException {
        if ( jolokia == null ){
            warn("Not connected.\n");
            return;
        }
        Pattern pattern = Pattern.compile(".*");
        if (args.size()>0) {
            pattern=Pattern.compile(String.join(" ",args) );
        }
        for (String val: jolokia.list(pattern)){
            info("%s\n",val);
        }
    }

    void exec(List<String> args) throws MessageSenderException {
        if ( jolokia == null ){
            warn("Not connected.\n");
            return;
        }
        Pattern pattern = Pattern.compile(".*");
        if (args.size()<2) {
            warn("Not enough arguments.\n");
            return;
        }
        String mbean = args.get(0);
        String operation = args.get(1);
        List<String> params = new ArrayList<>();
        for ( int i=2; i<args.size(); i++ ) params.add(args.get(i));
        info("%s\n", jolokia.exec(mbean, operation, params));
    }
    void spool(List<String> args) throws Exception {
        if (args.size()!=1){
            error("usage: spool file|off - outputs to file or stops spooling to file");
        }
        String file = args.get(0);
        if (fos != null ) fos.close();
        if ("off".equalsIgnoreCase(file)){
            this.out = System.out;
            fos = null;
        } else {
            fos = new FileOutputStream(file);
            this.out = new PrintStream( fos );
        }
        if ( browser != null ) browser.setOut(this.out);
        if ( receiver != null ) receiver.setOut(this.out);
    }

    void disconnect(){
        if(activeConnection==null) return;
        if ( sender != null ){
            sender.disconnect();
            sender = null;
        }
        if ( browser != null ){
            browser.disconnect();
            browser = null;
        }
        if ( receiver != null ){
            receiver.down();
            receiver = null;
        }
        info("Disconnected from %s\n", activeConnection);
        shell.setPrompt(null);
        activeConnection = null;
        jolokia = null;
    }

    void error(String message, Object...args){
        System.out.printf(Colors.RED+message+Colors.WHITE,args);
    }

    void info(String message, Object...args){
        System.out.printf(Colors.YELLOW+message+Colors.WHITE,args);
    }

    void warn(String message, Object...args){
        System.out.printf(Colors.PURPLE+message+Colors.WHITE,args);
    }

    void usage(){
        System.out.println(Colors.BLUE+USAGE+Colors.WHITE);
    }

}
