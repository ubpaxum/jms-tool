package org.ibro.jmstool.cli;


public class ShutdownHook extends Thread {
    CommandParser cm = null;
    public ShutdownHook(CommandParser cm ){
        this.cm = cm;
    }

    public void run() {
          if (cm!=null) cm.process("exit");
    }

}