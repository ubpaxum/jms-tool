package org.ibro.jmstool.cli;

/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2024-10-11
 */
public interface Colors {
    String BLACK = "\u001B[30m";
    String RED = "\u001B[31m";
    String GREEN = "\u001B[32m";
    String YELLOW = "\u001B[33m";
    String BLUE = "\u001B[34m";
    String PURPLE = "\u001B[35m";
    String CYAN = "\u001B[36m";
    String WHITE = "\u001B[37m";
    //Reset code
    String RESET = "\u001B[0m";
    
}
