package org.ibro.jmstool.cli;

import java.util.List;

/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2024-10-11
 */
public class BrowseOptions {
    public String queueName;
    public boolean isCount = false;
    public boolean isDelete = false;
    public String jmsFilter = null;
    public String regexFilter = null;
    public String status = null;

    public boolean parse(List<String> options) {
        if (options.isEmpty()) {
            status="Missing required parameter queue name";
            return false;
        }

        boolean jmsFilterSet = false;
        boolean regexFilterSet = false;
        for (String option: options ){
            if ( queueName == null ) {
                queueName = option;
            } else if ("--filter".equalsIgnoreCase(option)) {
                jmsFilterSet = true;
                regexFilterSet = false;
            } else if ("--regex".equalsIgnoreCase(option)) {
                jmsFilterSet = false;
                regexFilterSet = true;
            } else if ( jmsFilterSet ) {
                jmsFilter = jmsFilter==null?option:jmsFilter+" "+option;
            } else if ( regexFilterSet ) {
                regexFilter = regexFilter==null?option:regexFilter+" "+option;
            } else {
                status = "Unknown parameter/option "+option;
                return false;
            }
        }
        status="OK";
        return true;
    }
}
