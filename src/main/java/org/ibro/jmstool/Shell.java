package org.ibro.jmstool;

import org.ibro.jmstool.cli.CommandParser;
import org.ibro.jmstool.cli.ShutdownHook;
import org.ibro.jmstool.completer.ShellSyntaxCompleter;
import org.ibro.jmstool.config.JmsToolConfig;
import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.UserInterruptException;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 * JMS shell - command line interpreter using JMS API
 * User: ibro
 * Date: 11/10/2024
 * Time: 5:15 PM
 * JMS CLI
 */
public class Shell extends MainBase{
    public static final String GREEN = "\u001B[32m";
    public static final String WHITE = "\u001B[37m";
    //Reset code
    public static final String RESET = "\u001B[0m";

    private static final String USAGE =
            "Usage:\n"+
                    "jms-shell [-c <configFile>] [-v] [-h]\n" +
                    "\tOptions:\n" +
                    "\t-v|--version\t Show version information\n" +
                    "\t-h|--help\t Show this screen\n" +
                    "\t-c\t configuration file in YAML format - default is jmstool.yaml in $HOME|$CUR_DIR|$JMS_TOOL_HOME\n" ;
    private String promptColor = GREEN;
    private String textColor   = WHITE;

    private final static String COMMAND_LINE_PROMPT = "jms-shell";
    private final static String COMMAND_LINE_CONTINUE = "> ";

    private String promptString = null;
    private String continueString = null;
    // main input loop
    private Terminal terminal = null;
    private InputStream in ;
    private PrintStream out ;
    private JmsToolConfig config;
    private ShellSyntaxCompleter completer = null;

    public Shell(JmsToolConfig config) throws IOException {
        this.config = config;
        this.terminal = TerminalBuilder.terminal();
        this.out = new PrintStream( System.out );
        this.in  = terminal.input();
        this.promptString = promptColor+COMMAND_LINE_PROMPT+COMMAND_LINE_CONTINUE+textColor;
        this.continueString = promptColor+COMMAND_LINE_CONTINUE+textColor;

    }

    public void setPrompt(String prompt){
        if ( prompt == null ) prompt=COMMAND_LINE_PROMPT;
        this.promptString = promptColor+prompt+COMMAND_LINE_CONTINUE+textColor;
    }

    public static void main(String[] args) throws Exception{
        initLogger();
        String configFile = null;
        String app = null;
        boolean isConfig = false;
        for (String arg: args ) {
            if (arg.equals("-c")) {
                isConfig = true;
                continue;
            }
            if (arg.equals("-h") || arg.equals("--help")) {
                dieWithInvalidArgs("", USAGE);
            }

            if (arg.equals("-v") || arg.equals("--version")) {
                showVersion("jms-shell");
            }

            if (isConfig) {
                configFile = arg;
                isConfig = false;
                continue;
            }

        }
        JmsToolConfig config = null;
        File fConfig  = getConfigFile(configFile);
        if ( fConfig == null )
            dieWithInvalidArgs("Configuration file not specified or cannot be read", USAGE);

        try (InputStream is = new FileInputStream( fConfig )){
            config = new Yaml().loadAs(is,JmsToolConfig.class);
        }

        if ( app == null ) app = config.getDefaultConnection();

        if ( app == null )
            dieWithInvalidArgs("Active configuration not specified and not set in configuration file!", USAGE);

        int resultCode = 0;
        // disbale jline logger
        disableLogger();
        Shell shell = null;
        try{
            shell = new Shell(config);
            Logger log =  LoggerFactory.getLogger(Shell.class);
            shell.readLoop();
        } catch (IOException e){
            e.printStackTrace();
            resultCode = -1;
        } finally {
            if (shell!=null) shell.stop();
        }
        System.exit(resultCode);

    }

    public void readLoop() throws IOException {

        int cpRes = 0;
        this.completer = new ShellSyntaxCompleter(config);
        completer.setAutocompleteConnectionNames(new ArrayList<>(config.getConnections().keySet()));
        String line=null;
        LineReader lineReader = LineReaderBuilder.builder()
//                        .completer(new SqlDrusCompleter(sqlDrus.getConfig()))
                .completer(completer)
//                        .history(new DefaultHistory())
//                        .highlighter(new DefaultHighlighter())
                .terminal(terminal)
                .build();
        lineReader.setVariable(LineReader.HISTORY_FILE, new File(config.getHistoryFile()) );

        CommandParser parser = new CommandParser(this);
        ShutdownHook shutdownHook = new ShutdownHook(parser);
        Runtime.getRuntime().addShutdownHook(shutdownHook);
        try {
            while (cpRes >= 0 && (line = lineReader.readLine((cpRes > 0) ? getContinuePrompt() : getPrompt())) != null) {
                cpRes = parser.process(line);
            }
        } catch (UserInterruptException e) {
            parser.process("exit");
        } catch (EndOfFileException e) {
            parser.process("exit");
        }finally {
            stop();
        }
    }


    public void stop(){
        if ( terminal != null ) terminal.flush();
        if ( out != null )  out.close();
    }

    public String getPrompt(){
        return this.promptString;
    }

    public String getContinuePrompt(){
        return continueString;
    }


    private static void disableLogger(){
        java.util.logging.Logger jlineLogger = java.util.logging.Logger.getLogger("org.jline");
        if (jlineLogger != null) jlineLogger.setLevel(Level.OFF);
    }

    public InputStream getIn() {
        return in;
    }

    public PrintStream getOut() {
        return out;
    }

    public JmsToolConfig getConfig() {
        return config;
    }

    public Terminal getTerminal() {
        return terminal;
    }


    public ShellSyntaxCompleter getCompleter() {
        return completer;
    }
}
