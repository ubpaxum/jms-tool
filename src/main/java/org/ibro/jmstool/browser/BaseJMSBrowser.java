package org.ibro.jmstool.browser;

import org.ibro.jmstool.QueueConnectionWrapper;
import org.ibro.jmstool.sender.JMSSender;
import org.ibro.jmstool.sender.MessageSenderException;
import org.ibro.jmstool.util.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import javax.jms.Queue;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.ibro.jmstool.QueueConstants.DEFAULT_CONNECT_RETRY_DELAY;
import static org.ibro.jmstool.QueueConstants.DEFAULT_MAX_CONNECT_RETRIES;

/**
 * Basic implementation of JMS sender
 * @author ibro
 * Date: 03/06/14
 * Time: 1:54 PM.
 */
public class BaseJMSBrowser {

    /** The logger. */
    protected static final Logger log                 = LoggerFactory.getLogger(BaseJMSBrowser.class);

    /** The queue connection */
    protected Connection conn                = null;

    /** The connection string. */
    protected String connectionString             = null;

    /** The connection factory. */
    protected String connectionFactory            = null;

    /** The browse queue name. */
    protected String browseQueueName = null;

    /** The browse topic name. */
    protected String browseTopicName = null;

    /** The environment parameters map */
    protected Hashtable<String, String> env       = null;

    /** The boolean that keeps information if it is connected. */
    protected boolean isConnected                 = false;

    /** The retry count. */
    protected int retryCount                      = 0;

    /** The retry delay. */
    protected int retryDelay                       = 0;

    /** The delivery mode. */
    protected  int deliveryMode                     = DeliveryMode.PERSISTENT;

    protected JMSSender.MessageMode messageMode               = JMSSender.MessageMode.BYTE;

    private boolean isDelete = false;
    private boolean isCount = false;
    private String filter;
    private String regexFilter;
    private Pattern pattern;

    protected PrintStream out;


    public BaseJMSBrowser() {
        this.out = System.out;
    }

    /**
     * Instantiates a new base JMS sender.
     *
     * @param connectionString The connection string
     * @param connectionFactory The connection factory
     * @param browseQueueName The send queue name
     * @param env The environment parameters map
     */
    public BaseJMSBrowser(String connectionString, String connectionFactory, String browseQueueName, Hashtable<String, String> env){
        this();
        this.connectionString    = connectionString;
        this.connectionFactory   = connectionFactory;
        this.browseQueueName = browseQueueName;
        this.env                 = env;
        retryCount =  DEFAULT_MAX_CONNECT_RETRIES ;
        retryDelay = DEFAULT_CONNECT_RETRY_DELAY;
        log.info( String.format("factory: %s, server: %s, queue: %s, env: %s",connectionFactory, connectionString, browseQueueName, env) );
    }

    public PrintStream getOut() {
        return out;
    }

    public void setOut(PrintStream out) {
        this.out = out;
    }

    public boolean isCount() {
        return isCount;
    }

    public void setCount(boolean count) {
        isCount = count;
    }

    /* (non-Javadoc)
         * @see org.ccn2.common.queue.JMSSender#isUp()
         */
    public boolean isUp(){
        return isConnected;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#getDeliveryMode()
     */
    public int getDeliveryMode() {
        return deliveryMode;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#setDeliveryMode(int)
     */
    public void setDeliveryMode(int deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#connect()
     */
    public boolean connect() {
        if (conn != null) return  true;
        isConnected = false;
        int failCount = 0;
        QueueConnectionWrapper connectionWrapper = new QueueConnectionWrapper(env,connectionFactory);
        while ( failCount < retryCount ) {
            try {
                conn = connectionWrapper.createConnection();
                conn.start();
                isConnected = true;
                return true;
            } catch (Exception e) {
                log.error("ClassCastException initializing connection to queue",e);
                failCount ++;
            }
            try {
                log.debug(String.format("failCount: %s sleep for %s ms",failCount, retryDelay) );
                Thread.sleep(retryDelay);
            } catch (InterruptedException e) {
                log.info("Sleep interrupted - exiting");
                break;
            }
        }
        if (failCount >= retryCount){
            log.error("Unable to connect to message queue after "+failCount +" attempts. Giving up!" );
        }
        log.info(String.format("Connected %s: %s",connectionFactory,isConnected));
        return isConnected;
    }


    /* (non-Javadoc)
        * @see org.ccn2.common.queue.JMSSender#send(java.io.InputStream)
        */
    public int browse() throws MessageSenderException {
        Session session = null;
        QueueSender browser = null;
        Topic topic = null;
        int messageCount = 0;
        try {
            if ( !connect() ) return 0;

            session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);

            log.debug("JMS session created.");
            if ( browseTopicName != null) {
                log.debug("Browse topic {}", browseTopicName);
                topic=session.createTopic(browseTopicName);
                MessageConsumer consumer = session.createConsumer(topic);
                Message message=consumer.receive(200);
                while (  message != null ) {
                   browseMessage(message);
                   messageCount ++;
                   message = consumer.receive(2000);
                }
            }
            if ( browseQueueName != null) {
                log.debug("Browse queue {}", browseQueueName);
                Queue queue = session.createQueue(browseQueueName);
                QueueBrowser queueBrowser = session.createBrowser(queue, filter);
                messageCount += browseQueueEnumeration( queueBrowser.getEnumeration(), session, queue, isDelete );
            }

        } catch (Exception e) {
            log.error("Exception browsing message from {}/{}",browseQueueName, browseTopicName);
            throw new MessageSenderException(e);
        } finally {
            try {
                if (browser != null ) browser.close();
                if (session != null) session.close();
            } catch (Exception ignore) {}
            //disconnect();
        }
        return messageCount;
    }

    protected int browseQueueEnumeration(Enumeration<Message> messages,Session session, Queue browseQueue, boolean isDelete) throws JMSException {
        int messageCount = 0;
        List<String> deleteFilters = new ArrayList<>();
        while (messages.hasMoreElements()) {
            Message message = messages.nextElement();
            if ( contentMatching(message) ){
                browseMessage(message);
                messageCount++;
                if ( isDelete ){
                    deleteFilters.add("JMSTimestamp="+message.getJMSTimestamp() );
                }
            }
        }
        for (String filter: deleteFilters){
            log.info("Delete message {}", filter);
            MessageConsumer consumer = session.createConsumer(browseQueue,filter);
            Message m = consumer.receive(200);
            if (m !=null ) log.info("Deleted message with id: {}", m.getJMSMessageID());
        }
        return messageCount;
    }

    protected int consumeMessages(MessageConsumer consumer) throws JMSException {
        int messageCount = 0;
        Message message = consumer.receive(200);
        while (message != null) {
            if ( contentMatching(message) ){
                String jmsId = message.getJMSMessageID();
                browseMessage(message);
                message.acknowledge();
                out.printf("Deleted message with ID %s.\n", jmsId);
                messageCount++;
            }
            // read next message if any
            message = consumer.receive(200);
        }
        return messageCount;
    }

    protected void browseMessage(Message message) throws JMSException {
        if (!isCount) dumpAsMessageFormat(message);
    }


    /* (non-Javadoc)
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable {
        disconnect();
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#disconnect()
     */
    public void disconnect(){
        if (conn != null) {
            try {
                conn.close();
                log.info("JMS connection to "+ browseQueueName +" closed.");
            } catch (JMSException e) {
                log.error( String.format("Can't close JMS connection: %s",e.getMessage()));
            }
            conn = null;
        }
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#getRetryCount()
     */
    public int getRetryCount() {
        return retryCount;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#setRetryCount(int)
     */
    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#getRetryDelay()
     */
    public int getRetryDelay() {
        return retryDelay;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#setRetryDelay(int)
     */
    public void setRetryDelay(int retryDelay) {
        this.retryDelay = retryDelay;
    }

    public String getQueueName() {
        return browseQueueName;
    }

    public void setQueueName(String queueName){
        this.browseQueueName = queueName;
    }

    public void setBrowseTopicName(String browseTopicName) {
        this.browseTopicName = browseTopicName;
    }

    public JMSSender.MessageMode getMessageMode() {
        return messageMode;
    }

    public void setMessageMode(JMSSender.MessageMode messageMode) {
        this.messageMode = messageMode;
    }


    private void dumpAsMessageFormat(Message message) throws JMSException {
        out.printf("Uploaded at %s\n", new Date(message.getJMSTimestamp()) );
        out.printf("==> BEGIN HEADER ==>\n");
        out.printf("JMSType: %s\n", getMessageType(message));
        out.printf("JMSMessageID: %s\n", message.getJMSMessageID() );
        out.printf("JMSTimestamp: %s\n", message.getJMSTimestamp() );
        if ( message.getJMSCorrelationID() != null )
            out.printf("JMSCorrelationID: %s\n", message.getJMSCorrelationID() );
        dumpHeaders(message);
        out.printf("==> END HEADER ==>\n");
        out.printf("==> BEGIN MESSAGE ==>\n");
        out.printf("%s\n", getMessageContent(message) );
        out.printf("==> END MESSAGE ==>\n", message.getJMSMessageID() );
    }

    private void dumpHeaders(Message message) throws JMSException {
        Enumeration<String> headers = message.getPropertyNames();
        while ( headers.hasMoreElements() ){
            String propName = headers.nextElement();
            out.printf("%s: %s\n", propName, message.getStringProperty(propName) );
        }
    }

    private String getMessageContent(Message message) throws JMSException {
        if ( message instanceof TextMessage )
            return  ((TextMessage)message).getText();
        else if ( message instanceof BytesMessage )
            return StreamUtils.byteArrayToString(toByteArray((BytesMessage)message));
        return "__NOT_IMPLEMENTED__";
    }

    private boolean contentMatching(Message message) throws JMSException {
        if ( pattern == null ) return true;
        return pattern.matcher( getMessageContent(message) ).find();
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getRegexFilter() {
        return regexFilter;
    }

    public void setRegexFilter(String regexFilter) {
        this.regexFilter = regexFilter;
        if ( regexFilter==null || regexFilter.isEmpty() ){
            this.pattern = null;
        } else {
            this.pattern = Pattern.compile(regexFilter);
        }
    }

    public byte[] toByteArray(BytesMessage bytesMessage) throws JMSException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer = new byte[2048];
        int read = 0;
        while ( (read=bytesMessage.readBytes(buffer)) > 0 ){
            bos.write(buffer,0,read);
        }
        return bos.toByteArray();
    }

    public InputStream toInputStream( BytesMessage bytesMessage ) throws JMSException {
        return new ByteArrayInputStream( toByteArray(bytesMessage) );
    }

    public String getMessageType(Message message){
        if ( message instanceof BytesMessage ) return "BytesMessage";
        if ( message instanceof TextMessage ) return "TextMessage";
        if ( message instanceof MapMessage ) return "MapMessage";
        if ( message instanceof ObjectMessage ) return "ObjectMessage";
        return "==unknown==";
    }
}
