package org.ibro.jmstool.browser;

import org.ibro.jmstool.QueueConstants;
import org.ibro.jmstool.config.JmsConnectionSetting;

import javax.naming.Context;
import java.util.Hashtable;

public class BrowserFactory {

    public static BaseJMSBrowser createBrowser(JmsConnectionSetting setting, String queueName){
        Hashtable env = new Hashtable<String, String>();
        String jmsType = setting.getType();
        env.put(QueueConstants.PROP_JMS_TYPE, jmsType);
        env.put(Context.PROVIDER_URL, setting.getUrl() );
        if ( jmsType == null || jmsType.equals("context")){
            env.put(Context.INITIAL_CONTEXT_FACTORY, setting.getContext());
            if ( setting.getPrefix() != null ) {
                env.put(Context.URL_PKG_PREFIXES, setting.getPrefix());
            }
        }
        if ( setting.getUserName() != null ) {
            env.put(Context.SECURITY_PRINCIPAL, setting.getUserName() );
            env.put(Context.SECURITY_CREDENTIALS, setting.getUserPassword() );
        }

        return new BaseJMSBrowser("", setting.getFactory(), queueName, env);

    }
}
