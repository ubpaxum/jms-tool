package org.ibro.jmstool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 6/9/15
 * Time: 5:15 PM
 * Short description of purpose.
 */
public class MainBase {
    protected static Logger log ;
    protected static final String DEFAULT_CONF_FILE = "jmstool.yaml";

    protected static void initLogger(){
        System.setProperty("slf4j.internal.verbosity", "WARN");
        log = LoggerFactory.getLogger(MainBase.class);
    }

    protected static File getConfigFile(String configFile){
        File fConfig = null;
        if ( configFile == null || configFile.isEmpty() ){
            configFile= System.getProperty("user.dir") + File.separator+ DEFAULT_CONF_FILE;
            fConfig = getFile(configFile);

            if ( fConfig == null ){
                configFile= System.getProperty("user.home") + File.separator+ DEFAULT_CONF_FILE;
                fConfig = getFile(configFile);
            }

            if ( fConfig == null ){
                configFile= getJmsToolHome() + File.separator+ DEFAULT_CONF_FILE;
                fConfig = getFile(configFile);
            }

        } else {
            fConfig = getFile(configFile);
        }
        return fConfig;
    }

    protected static File getFile(String path){
        File fConfig = new File(path);
        if ( fConfig.exists() && fConfig.isFile() ) return fConfig;
        return null;

    }

    protected static void dieWithInvalidArgs(String message, String usage){
        System.err.printf("Error: %s\n",message);
        System.err.printf(usage);
        System.exit(-1);
    }

    protected static void showVersion(String toolName){
        System.err.printf("%s %s\n%s\n",toolName,Version.VERSION,Version.COPYRIGHT);
        System.exit(0);
    }

    protected static String getJmsToolHome(){
        final File f = new File(MainBase.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        return extractMainPath(f.getAbsolutePath());
    }


    protected static String extractMainPath(String path){
        if ( path.endsWith(".jar") ){
            Pattern p = Pattern.compile("^([\\\\A-Za-z0-9_/:. -]+)(jms-tool-[0-9a-z_.-]+\\.jar)",Pattern.UNICODE_CASE+Pattern.MULTILINE);
            Matcher m = p.matcher(path);
            if ( m.find() ){
                String pth = m.group(1);
                if ( pth.endsWith("/lib/") || pth.endsWith("\\lib\\") ) pth = pth.substring(0, pth.length()-5 );
                return pth;
            }
        }
        return path;
    }
}
