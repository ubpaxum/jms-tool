package org.ibro.jmstool;


/**
 * QueueConstants for the org.ccn2.common.queue package
 */
public interface QueueConstants {

    public static final String PROP_JMS_TYPE = "jms.type";

    // status of Receiver
    /** The status meaning JMS receiver is not connected and not shut down. */
    public static final int    STATUS_DISCONNECTED         = 0;

    /** The status meaning JMS receiver is connected and listening. */
    public static final int    STATUS_CONNECTED            = 1;

    /** The status meaning JMS receiver is down. */
    public static final int    STATUS_DOWN                 = 2;

    /** Config item name for maximum connection retry attempts */
    public static final String PROR_MAX_CONNECT_RETRIES= "jms.max.connect.retries";

    /** Config item name for retry delay */
    public static final String PROR_CONNECT_RETRY_DELAY = "jms.connect.retry.delay";

    /** Config item name for poll interval */
    public static final String PROR_QUEUE_POLL_INTERVAL = "jms.queue.poll.interval" ;

    /**  Default values for configuration properties. */
    /** Default number of maximum connection retry attempts */
    public static final int DEFAULT_MAX_CONNECT_RETRIES= 1440;

    /** Default delay (in ms) between connect retry. */
    public static final int DEFAULT_CONNECT_RETRY_DELAY = 60000;

    /** Default poll interval (in ms) for polling the input queue. */
    public static final int DEFAULT_QUEUE_POLL_INTERVAL = 5000 ;


    public static final String JMS_CCN_CORREL_ID           = "CCN_CORREL_ID";
    public static final String JMS_CD_FEEDBACK             = "IE_MSG_CD_FEEDBACK";

    public static final String ECS2_JMS_CORREL_ID           = "jms_correl_id";

}