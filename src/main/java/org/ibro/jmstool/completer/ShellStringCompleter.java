package org.ibro.jmstool.completer;

import org.ibro.jmstool.config.JmsToolConfig;
import org.jline.reader.Candidate;
import org.jline.reader.impl.completer.StringsCompleter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/15/16
 * Time: 5:06 PM
 * Short description of purpose.
 */
public class ShellStringCompleter extends StringsCompleter {
    private final List<String> keywords;
    public ShellStringCompleter(JmsToolConfig config) {
        super();
        keywords = new ArrayList<>();
        try {
            load(ShellStringCompleter.class.getResourceAsStream("/" + JmsToolConfig.KEYWORDS_FILE));
            for (String f: config.getKeywordsFiles() ){
                    InputStream is = new FileInputStream(f);
                    load( is );
                    is.close();
            }
        } catch (IOException e) {
            //log.error("Error", e);
        }
    }

    private void load(InputStream stream) throws IOException {
        BufferedReader r = new BufferedReader( new InputStreamReader(stream) );
        String line;
        while ( (line=r.readLine()) != null ){
            candidates.add( new Candidate(line) );
            keywords.add(line.trim().toLowerCase());
        }
    }

    public List<String> getKeywords() {
        return keywords;
    }
}
