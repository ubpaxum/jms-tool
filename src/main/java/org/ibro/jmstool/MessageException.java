package org.ibro.jmstool;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 1/26/15
 * Time: 12:26 PM
 * Short description of purpose.
 */
public class MessageException extends Exception {

    public MessageException() {
        super();
    }

    public MessageException(String message) {
        super(message);
    }

    public MessageException(String message, Throwable cause) {
        super(message, cause);
    }

    public MessageException(Throwable cause) {
        super(cause);
    }

    protected MessageException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
