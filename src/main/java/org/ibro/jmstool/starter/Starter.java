package org.ibro.jmstool.starter;

import org.ibro.jmstool.receiver.BaseJMSReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 6/11/15
 * Time: 11:23 AM
 * Short description of purpose.
 */
public class Starter extends Thread {
    private static Logger log = LoggerFactory.getLogger(Starter.class);
    private static final int SLEEP_INTERVAL = 60 * 1000;
    private static final int SHUTDOWN_SLEEP_INTERVAL = 2 * 1000;

    private Map<String, BaseJMSReceiver> listeners;

    public Starter() {
        listeners = new HashMap<String, BaseJMSReceiver>();
    }

    public void addListener(String name, BaseJMSReceiver listener) {
        listeners.put(name, listener);
    }

    public void start() {
        for (String listenerName : listeners.keySet()) {
            log.info(String.format("Starting listener %s ... ", listenerName));
            BaseJMSReceiver listener = listeners.get(listenerName);
            listener.setDaemon(true);
            listener.start();
            log.info("done.");
        }

        // empty sleep loop
        while (true) {
            try {
                Thread.sleep(SLEEP_INTERVAL);
            } catch (InterruptedException e) {
                log.info("Interrupted - gracefully shutdown", e);
                break;
            }
        }
    }

    public void end() {
        for (String listenerName : listeners.keySet()) {
            log.info(String.format("Stopping listener %s ... ", listenerName));
            BaseJMSReceiver listener = listeners.get(listenerName);
            listener.interrupt();
        }

        // give some time
        log.info(String.format("Interrupt main thread and sleep %s for graceful shutdown", SHUTDOWN_SLEEP_INTERVAL));
        sleep(SHUTDOWN_SLEEP_INTERVAL);

        // finally interrupt this thread
        Thread.currentThread().interrupt();
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(SHUTDOWN_SLEEP_INTERVAL);
        } catch (InterruptedException e) {
        }
    }
}
