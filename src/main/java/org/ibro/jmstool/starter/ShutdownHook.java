package org.ibro.jmstool.starter;

public class ShutdownHook extends Thread{
    private Starter starter;
    public ShutdownHook( Starter starter) {
        this.starter = starter;
    }

    @Override
    public void run() {
        System.out.println("Inside Add Shutdown Hook");
        if ( starter != null ) starter.end();
        System.out.println("Gracefully shutdown.");
    }

}

