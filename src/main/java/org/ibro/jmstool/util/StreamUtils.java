package org.ibro.jmstool.util;

import org.hornetq.utils.Base64;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import java.io.*;
import java.sql.Blob;
import java.sql.Clob;

public class StreamUtils {
    private static final int BUFFER_SIZE = 8196;

    public static byte[] inputStreamToByteArray(InputStream is) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer = new byte[BUFFER_SIZE];
        int read =0;
        while( (read=is.read(buffer)) > 0 ){
            bos.write(buffer,0,read);
        }
        return bos.toByteArray();
    }

    public static String inputStreamToString(InputStream is, boolean isBinary) throws IOException{
        byte[] bytes = inputStreamToByteArray(is);
        if ( isBinary ) return Base64.encodeBytes(bytes);
        return byteArrayToString(bytes);
    }

    public static String readerToString(Reader r) throws IOException{
        char[] arr = new char[BUFFER_SIZE];
        StringBuilder buf = new StringBuilder();
        int numChars;

        while ((numChars = r.read(arr, 0, arr.length)) > 0) {
            buf.append(arr, 0, numChars);
        }

        return buf.toString();
    }

    public static String inputStreamToString(InputStream is) throws IOException{
        return inputStreamToString(is, false);
    }

    public static InputStream byteArrayToInputStream(byte[] buffer) throws IOException {
        return new ByteArrayInputStream( buffer );
    }

    public static byte[] stringToByteArray(String str) {
        try {
            return str.getBytes("utf8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String byteArrayToString(byte[] bytes){
        try {
            return new String(bytes,"utf8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String clobToString(Object clob){
        if ( clob == null ) return null;
        else if ( clob instanceof InputStream ) {
            try {
                return inputStreamToString( (InputStream)clob, false );
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else if ( clob instanceof byte[] ) {
            return byteArrayToString( (byte[])clob );
        } else if ( clob instanceof Clob) {
            try {
                String val =readerToString( ((Clob) clob).getCharacterStream() );
                return val;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else if ( clob instanceof String ) {
            return (String) clob;
        }
        return null;
    }

    public static String blobToString(Object blob){
        if ( blob == null ) return null;
        else if ( blob instanceof InputStream ) {
            try {
                return inputStreamToString( (InputStream)blob, true );
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else if ( blob instanceof byte[] ) {
            return Base64.encodeBytes( (byte[])blob );
        } else if ( blob instanceof String ) {
            return (String) blob;
        } else if ( blob instanceof Blob) {
            try {
                return inputStreamToString( ((Blob)blob).getBinaryStream(), true );
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public static byte[] toByteArray(BytesMessage bytesMessage) throws JMSException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer = new byte[BUFFER_SIZE];
        int read = 0;
        while ( (read=bytesMessage.readBytes(buffer)) > 0 ){
            bos.write(buffer,0,read);
        }
        return bos.toByteArray();
    }

    public InputStream toInputStream( BytesMessage bytesMessage ) throws JMSException {
        return new ByteArrayInputStream( toByteArray(bytesMessage) );
    }

}
