package org.ibro.jmstool.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.*;

public class XmlUtils {
    private static final Logger log = LoggerFactory.getLogger(XmlUtils.class);
    private static XPathFactory XPATH_FACTORY      = XPathFactory.newInstance();

    /**
     * Creates empty XML document
     */
    public static Document createXmlDocument() {
        DocumentBuilder db = createDocumentBuilder();
        return db.newDocument();
    }

    private static DocumentBuilder createDocumentBuilder() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setValidating(false);
//        log.debug("DocumentBuilderFactory: {}", dbf);
        try {
            return dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            log.error("Error", e);
            return null;
        }

    }
    /**
     * Parses document from input stream and returns XML document
     * @param fis input stream object
     * @return XML document if input file is well formed
     * @throws IOException, SAXException if file is not found, readable, or not well-formed
     */
    public static Document parse(InputStream fis) throws SAXException, IOException{
        DocumentBuilder db = createDocumentBuilder();
        return db.parse(fis);
    }

    /**
     * Parse String with given encoding
     * @param str
     * @param encoding
     * @return
     * @throws SAXException
     * @throws IOException
     */
    public static Document parse(String str, String encoding) throws SAXException, IOException{
        return parse(new ByteArrayInputStream(str.getBytes(encoding)));
    }


    /**
     * Select sinle node by XPath expression
     * @param startFrom - the node to start search from
     * @param xpathExpr is XPath expression to be used for search
     * @param ctx namespace context to XML (for using prefixes). Null if None
     * @return Node object if found, or null if searched did not matched any node
     * @throws Exception if xpathExpr is not valid XPath
     */
    public static Node selectSingleNode(Node startFrom, String xpathExpr, NamespaceContext ctx) throws Exception{
        XPath xpath = XPATH_FACTORY.newXPath();
        if ( ctx != null ) xpath.setNamespaceContext( ctx );
        XPathExpression xexpr = xpath.compile(xpathExpr);
        Node node = (Node) xexpr.evaluate( startFrom, XPathConstants.NODE );
        return node;
    }

    public static Node selectSingleNode(Node startFrom, String xpathExpr) throws Exception{
        return selectSingleNode(startFrom ,xpathExpr, null);
    }

    public static void write(Document doc, StreamResult out, String encoding, boolean idented) {
        try {
            TransformerFactory tf = SAXTransformerFactory.newInstance();
            //tf.setAttribute("indent-number", new Integer(2));
            //enable the indent in the transformer
            Transformer t = tf.newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, (idented?"yes":"no") );
            if(idented) {
                t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            }
            t.setOutputProperty(OutputKeys.ENCODING, encoding);
            //(3)wrap the otuputstream with a writer (or bufferedwriter)
            t.transform(new DOMSource(doc), out);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Serialize Node (Document is included) to supplied output stream
     * @param node is th node to be serialized - it can be whole XML Document
     * @param out the output stream to write to
     */
    private static void serializeInternal(Node node, Object out){
        if (node == null) return;
        try {
            DOMImplementationRegistry registry =
                    DOMImplementationRegistry.newInstance();
            DOMImplementationLS impl =
                    (DOMImplementationLS)registry.getDOMImplementation("LS");
            if (impl != null){
                LSSerializer domWriter = impl.createLSSerializer();
                DOMConfiguration config = domWriter.getDomConfig();
                //config.setParameter("format-pretty-print", Boolean.TRUE);
                //domWriter.writeNode(System.out, doc);
                LSOutput dOut = impl.createLSOutput();
                if (out instanceof Writer)
                    dOut.setCharacterStream( (Writer) out);
                else if (out instanceof OutputStream)
                    dOut.setByteStream( (OutputStream) out);
                else
                    throw new Exception("Supplied object is neither OutputStream, nor Writer");
                domWriter.write(node,dOut);
            } else {
                if (out instanceof Writer)
                    ((Writer) out).append( node.toString() );
                else if (out instanceof OutputStream)
                    ( (OutputStream) out).write( node.toString().getBytes() );
                else
                    throw new Exception("Supplied object is neither OutputStream, nor Writer");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Serialize Node (Document is included) to supplied output stream
     * @param node is th node to be serialized - it can be whole XML Document
     * @param out the output stream to write to
     * @param idented if XML shall be pretty-idented
     */
    public static void serialize(Document node, OutputStream out, String encoding, boolean idented){
        try{
//            write(node, new StreamResult(new OutputStreamWriter(out, "utf-8")), idented);
            write(node, new StreamResult(out), encoding, idented);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Serialize Node (Document is included) to supplied output stream
     * @param node is th node to be serialized - it can be whole XML Document
     * @param writer the writer object to write to
     * @param idented if XML shall be pretty-idented
     */
    public static void serialize(Document node, Writer writer, String encoding,  boolean idented){
        write(node, new StreamResult(writer), encoding, idented );
    }

    /**
     * Serialize Node (Document is included) to supplied output stream
     * @param node is th node to be serialized - it can be whole XML Document
     * @param writer the writer object to write to
     */
    public static void serialize(Document node, Writer writer, String encoding){
        serialize(node, writer, encoding,false);
    }

    /**
     * Serialize Node (Document is included) to supplied output stream
     * @param node is th node to be serialized - it can be whole XML Document
     * @param out the output stream to write to
     */
    public static void serialize(Document node, OutputStream out, String encoding){
        serialize(node, out, encoding, false);
    }

    public static void write(Document document, OutputStream out, String encoding, boolean idented) throws Exception {
        TransformerFactory tf = SAXTransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, (idented?"yes":"no") );
        if(idented) {
            t.setOutputProperty( "{http://xml.apache.org/xslt}indent-amount", "4");
        }
        t.setOutputProperty(OutputKeys.ENCODING, encoding);
        //(3)wrap the otuputstream with a writer (or bufferedwriter)
        t.transform(new DOMSource(document), new StreamResult(out) );
    }

}
