package org.ibro.jmstool.util;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 6/2/15
 * Time: 12:31 PM
 * Short description of purpose.
 */
public class FileUtils {

    private static final int BUFFER_SIZE = 16*1024 ;

    /**
     * Reads GZIP-ed file and return the content as String
     * @param fileName
     * @return string content
     * @throws IOException
     */
    public static   String readGzippedFile( String fileName) throws IOException {
        StringBuilder result = new StringBuilder();

        FileInputStream fin = new FileInputStream(fileName);
        GZIPInputStream gzis = new GZIPInputStream(fin);
        InputStreamReader xover = new InputStreamReader(gzis);
        BufferedReader is = new BufferedReader(xover);

        String line;
        // Now read lines of text: the BufferedReader puts them in lines,
        // the InputStreamReader does Unicode conversion, and the
        // GZipInputStream "gunzip"s the data from the FileInputStream.
        while ((line = is.readLine()) != null) {
            result.append(line);
        }

        return result.toString();

    }


    /**
     * Writes string content in gzipped file.
     * @param fl file to write to
     * @param content
     * @throws IOException
     */

    public static void writeGzippedFile(File fl, byte[] content) throws IOException {
        OutputStream fos = new FileOutputStream(fl);

        GZIPOutputStream gzos = new GZIPOutputStream(fos);
        gzos.write(content);
        // now finish compressing
        gzos.finish();
        fos.close();
    }

    /**
     * Writes string content in gzipped file.
     * @param fl file to write to
     * @param content
     * @throws IOException
     */

    public static void writeGzippedFile(File fl, String content) throws IOException {
        writeGzippedFile(fl, content.getBytes("UTF-8"));
    }

    /**
     * Writes the string content in gzipped file
     * @param fileDir  - directory to write to
     * @param fileName - file name
     * @param content  - string content
     * @throws IOException
     */
    public static void writeGzippedFile(File fileDir, String fileName, String content) throws IOException {
        writeGzippedFile(new File(fileDir,fileName),content);
    }

    public static File writeToTempFile(InputStream is, String extension) throws IOException {
        File tempFile = File.createTempFile("TMP",extension);
        FileOutputStream fos = new FileOutputStream( tempFile );
        byte[] buffer = new byte[BUFFER_SIZE];
        int read = -1;
        while ( (read=is.read(buffer)) > 0  ){
            fos.write(buffer,0,read);
        }
        fos.close();
        return tempFile;
    }

}
