package org.ibro.jmstool.util;

import org.ibro.jmstool.model.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 8/10/15
 * Time: 5:17 PM
 * Read messages from single file with specified format
 * TODO: describe format
 */
public class MessageFormatReader {
    private static final Logger log = LoggerFactory.getLogger(MessageFormatReader.class);
    private String fileName;
    private List<MessageModel> messages;

    public MessageFormatReader( String fileName ) {
        this.fileName = fileName;
        this.messages = new ArrayList<MessageModel>();
    }


    public void loadMessages() throws IOException {
        Reader fr = new FileReader(fileName);
        BufferedReader reader = new BufferedReader( new FileReader(fileName) );
        String line = null;

        MessageModel model = null;
        boolean isHeader = false;
        boolean isBody = false;
        while ( (line=reader.readLine()) != null ){
            line = line.trim();
            log.debug("Inspect line "+line);
            if ( line.equals("==> BEGIN HEADER ==>") ){
                model = new MessageModel();
                isHeader = true;
            } else if ( line.equals("==> END HEADER ==>") ){
                isHeader = false;
            } else if ( line.equals("==> BEGIN MESSAGE ==>") ){
                isBody = true;
            } else if ( line.equals("==> END MESSAGE ==>") ){
                isBody = false;
                messages.add(model);
            } else if ( isHeader ){
                int idx = line.indexOf(':');
                if ( idx < 2 )
                    throw new RuntimeException("Invalid format of heading. Format is HEADING: HEADING-VALUE");

                String heading = line.substring(0,idx);
                String value = null;
                if ( idx+1 < line.length() ) // no value for heading - set it as null
                    value = line.substring(idx+1).trim();

                if ( "".equals(value) ) value = null;

                if ( heading.equals("JMSType") )
                    model.setMessageType(value);
                else
                    model.addHeader(heading, value);
            } else if ( isBody ){
                model.addLine(line);
            }
        }
        fr.close();
        reader.close();
    }

    public List<MessageModel> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageModel> messages) {
        this.messages = messages;
    }

}
