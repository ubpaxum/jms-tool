package org.ibro.jmstool.config;

public class ForwardSetting {
    private boolean enabled = false;
    private String listenConnection;
    private String listenQueue;
    private String forwardConnection;
    private String forwardQueue;

    public ForwardSetting() {
    }

    public String getListenConnection() {
        return listenConnection;
    }

    public void setListenConnection(String listenConnection) {
        this.listenConnection = listenConnection;
    }

    public String getListenQueue() {
        return listenQueue;
    }

    public void setListenQueue(String listenQueue) {
        this.listenQueue = listenQueue;
    }

    public String getForwardConnection() {
        return forwardConnection;
    }

    public void setForwardConnection(String forwardConnection) {
        this.forwardConnection = forwardConnection;
    }

    public String getForwardQueue() {
        return forwardQueue;
    }

    public void setForwardQueue(String forwardQueue) {
        this.forwardQueue = forwardQueue;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "ForwardSetting{" +
                "listenConnection='" + listenConnection + '\'' +
                ", listenQueue='" + listenQueue + '\'' +
                ", forwardConnection='" + forwardConnection + '\'' +
                ", forwardQueue='" + forwardQueue + '\'' +
                ", enabled=" + enabled +
                '}';
    }
}
