package org.ibro.jmstool.config;

import org.ibro.jmstool.QueueConstants;

import java.util.Map;

public class JmsConnectionSetting {
    private String type;
    private String url;
    private String factory;
    private String context;
    private String userName;
    private String userPassword;
    private String prefix;
    private String origin;
    private String jolokiaBase;
    private String pattern;
    private int retryCount = QueueConstants.DEFAULT_MAX_CONNECT_RETRIES;
    private int retryDelay = QueueConstants.DEFAULT_CONNECT_RETRY_DELAY;
    Map<String,String> contextProperties;

    public JmsConnectionSetting() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public int getRetryDelay() {
        return retryDelay;
    }

    public void setRetryDelay(int retryDelay) {
        this.retryDelay = retryDelay;
    }

    public Map<String, String> getContextProperties() {
        return contextProperties;
    }

    public void setContextProperties(Map<String, String> contextProperties) {
        this.contextProperties = contextProperties;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getJolokiaBase() {
        return jolokiaBase;
    }

    public void setJolokiaBase(String jolokiaBase) {
        this.jolokiaBase = jolokiaBase;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
