package org.ibro.jmstool.config;

import ch.qos.logback.core.PropertyDefinerBase;
import org.ibro.jmstool.MainBase;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Describe the purpose of the class
 *
 * @author ibro
 * created 2023-05-19
 */
public class LogDirPropertyDefiner extends PropertyDefinerBase {
    @Override
    public String getPropertyValue() {
        return getJmsToolHome()+File.separator+"log";
    }

    private   String getJmsToolHome(){
        final File f = new File(MainBase.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        return extractMainPath(f.getAbsolutePath());
    }


    private   String extractMainPath(String path){
        if ( path.endsWith(".jar") ){
            Pattern p = Pattern.compile("^([\\\\A-Za-z0-9_/:. -]+)(jms-tool-[0-9a-z_.-]+\\.jar)",Pattern.UNICODE_CASE+Pattern.MULTILINE);
            Matcher m = p.matcher(path);
            if ( m.find() ){
                String pth = m.group(1);
                if ( pth.endsWith("/lib/") || pth.endsWith("\\lib\\") ) pth = pth.substring(0, pth.length()-5 );
                return pth;
            }
        }
        return path;
    }
}
