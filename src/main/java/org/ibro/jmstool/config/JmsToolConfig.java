package org.ibro.jmstool.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JmsToolConfig {
    public static final String KEYWORDS_FILE = "jmstool.keywords";
    public static final String SYNTAX_FILE   = "jmstool.syntax";
    public static final String HISTORY_FILE      = ".jmstool.history";
    private Map<String, JmsConnectionSetting> connections;
    private Map<String, ForwardSetting> forwarders;
    private Map<String, ListenerSetting> listeners;
    private String defaultConnection;

    private Set<String> configDirs ;
    private String      shellHome = null;

    public JmsToolConfig() {
    }

    public Map<String, JmsConnectionSetting> getConnections() {
        return connections;
    }

    public void setConnections(Map<String, JmsConnectionSetting> connections) {
        this.connections = connections;
    }

    public JmsConnectionSetting getConnectionSetting(String name){
        return connections.get(name);
    }

    public String getDefaultConnection() {
        return defaultConnection;
    }

    public void setDefaultConnection(String defaultConnection) {
        this.defaultConnection = defaultConnection;
    }

    public Map<String, ForwardSetting> getForwarders() {
        return forwarders;
    }

    public Map<String, ListenerSetting> getListeners() {
        return listeners;
    }

    public void setForwarders(Map<String, ForwardSetting> forwarders) {
        this.forwarders = forwarders;
    }

    public void setListeners(Map<String, ListenerSetting> listeners) {
        this.listeners = listeners;
    }

    public List<String> getKeywordsFiles(){
        return getFileList(KEYWORDS_FILE);
    }


    public String getHistoryFile(){
        return System.getProperty("user.home")+"/"+HISTORY_FILE;
    }

    private List<String> getFileList(String fileName) {
        List<String> files = new ArrayList<String>();
        for ( String dir: configDirs ){
            File directory = new File(dir);
            if ( directory.exists() && directory.canRead()  ){
                File file = new File(dir,fileName);
                if ( file.exists() && file.canRead() )
                    files.add( file.getAbsolutePath() );
            }
        }
        return files;
    }
}
