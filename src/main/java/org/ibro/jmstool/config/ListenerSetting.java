package org.ibro.jmstool.config;

public class ListenerSetting {
    private boolean enabled = false;
    private String listenConnection;
    private String listenQueue;
    private String topic;
    private String listenerClass;
    private String path;

    public ListenerSetting() {
    }

    public String getListenConnection() {
        return listenConnection;
    }

    public void setListenConnection(String listenConnection) {
        this.listenConnection = listenConnection;
    }

    public String getListenQueue() {
        return listenQueue;
    }

    public void setListenQueue(String listenQueue) {
        this.listenQueue = listenQueue;
    }

    public String getListenerClass() {
        return listenerClass;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setListenerClass(String listenerClass) {
        this.listenerClass = listenerClass;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "ListenerSetting{" +
                "listenConnection='" + listenConnection + '\'' +
                ", listenQueue='" + listenQueue + '\'' +
                ", topic='" + topic + '\'' +
                ", listenClass='" + listenerClass + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
