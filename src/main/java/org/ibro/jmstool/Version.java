package org.ibro.jmstool;
import java.io.IOException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 11/25/16
 * Time: 11:25 AM
 * Short description of purpose.
 */
public class Version {
    public static String VERSION = "0.0-snapshot";

    static {
        try {
            Properties props = new Properties();
            props.load(Version.class.getResourceAsStream("/version.properties"));
            VERSION = props.getProperty("version").replaceAll("\\\"","");
        } catch (IOException ignore) {
        }
    }



    public static final String OFFICIAL_NAME = "jms-tool";

    public static final String COPYRIGHT  = "Copyleft (c) by ibro (ubpaxum@gmail.com) under GPLv3.";

    public static final String VERSION_MESSAGE =
            String.format("%s v%s\n%s",OFFICIAL_NAME,VERSION,COPYRIGHT);
}
