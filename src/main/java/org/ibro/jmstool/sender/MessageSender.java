package org.ibro.jmstool.sender;

import java.io.InputStream;

/**
 * Created by bily on 5/26/15.
 */
public interface MessageSender {

    /**
     * Send message as bytes message.
     *
     * @param message The message to be sent
     * @return True, if successful
     * @throws Exception the exception
     */
    boolean send(InputStream message) throws MessageSenderException;


}
