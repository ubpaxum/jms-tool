package org.ibro.jmstool.sender;

import java.io.InputStream;
import java.util.Map;

/**
 * Basic functionality for JMS sender...
 * @author ibro
 * Date: 7/4/14
 * Time: 2:51 PM
 */
public interface JMSSender extends MessageSender {

    public enum MessageMode {TEXT, MessageMode, BYTE};

    /**
     * Checks if is up.
     *
     * @return True, if is up
     */
    boolean isUp();

    /**
     * Gets the delivery mode.
     *
     * @return The delivery mode
     */
    int getDeliveryMode();

    /**
     * Sets the delivery mode.
     *
     * @param deliveryMode The new delivery mode
     */
    void setDeliveryMode(int deliveryMode);

    /**
     * Connect.
     *
     * @return True, if successful
     */
    boolean connect();


    /**
     * Disconnect.
     */
    void disconnect();

    /**
     * Sets the retry count.
     *
     * @param retryCount The new retry count
     */
    void setRetryCount(int retryCount);

    /**
     * Gets the retry count.
     *
     * @return The retry count
     */
    int getRetryCount();

    /**
     * Sets the retry delay.
     *
     * @param ms The new retry delay
     */
    void setRetryDelay(int ms);

    /**
     * Gets the retry delay.
     *
     * @return The retry delay
     */
    int getRetryDelay();

    String getQueueName();

    boolean send(InputStream message, Map<String,Object> headers) throws MessageSenderException;

    boolean send(InputStream message) throws MessageSenderException;

    void setMessageMode( MessageMode mode);

    void setQueueName(String sendQueueName);

    String getTopicName();

    void setTopicName(String sendTopicName);

    MessageMode getMessageMode();




    }
