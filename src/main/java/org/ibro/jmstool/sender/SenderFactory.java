package org.ibro.jmstool.sender;

import org.ibro.jmstool.config.JmsConnectionSetting;

public class SenderFactory extends BaseFactory {

    public static BaseJMSSender createSender(JmsConnectionSetting setting, String queueName){
        BaseJMSSender sender =  new BaseJMSSender("", setting.getFactory(), queueName, createEnv(setting));
        sender.setRetryCount(setting.getRetryCount());
        sender.setRetryDelay(setting.getRetryDelay());
        return sender;
    }

    public static BaseJMSSender createTopicSender(JmsConnectionSetting setting, String topicName){
        BaseJMSSender sender = new BaseJMSSender("", setting.getFactory(), null, createEnv(setting));
        sender.setTopicName(topicName);
        sender.setRetryCount(setting.getRetryCount());
        sender.setRetryDelay(setting.getRetryDelay());
        return sender;
    }
}
