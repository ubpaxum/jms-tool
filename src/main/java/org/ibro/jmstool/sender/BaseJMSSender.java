package org.ibro.jmstool.sender;

import org.ibro.jmstool.QueueConnectionWrapper;
import org.ibro.jmstool.model.MessageModel;
import org.ibro.jmstool.util.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Map;

import static org.ibro.jmstool.QueueConstants.DEFAULT_CONNECT_RETRY_DELAY;
import static org.ibro.jmstool.QueueConstants.DEFAULT_MAX_CONNECT_RETRIES;

/**
 * Basic implementation of JMS sender
 * @author ibro
 * Date: 03/06/14
 * Time: 1:54 PM.
 */
public class BaseJMSSender implements JMSSender {

    /** The Constant buffer length. */
    public static final int            BUFLEN              = 65536;

    /** The logger. */
    protected static final Logger log                 = LoggerFactory.getLogger(BaseJMSSender.class);

    /** The queue connection */
    protected Connection conn                     = null;

    /** The connection string. */
    protected String connectionString             = null;

    /** The connection factory. */
    protected String connectionFactory            = null;

    /** The send queue name. */
    protected String sendQueueName                = null;

    /** The send queue name. */
    protected String sendTopicName                = null;

    /** The environment parameters map */
    protected Hashtable<String, String> env       = null;

    /** The boolean that keeps information if it is connected. */
    protected boolean isConnected                 = false;

    /** The retry count. */
    protected int retryCount                      = DEFAULT_MAX_CONNECT_RETRIES;

    /** The retry delay. */
    protected int retryDelay                       = DEFAULT_CONNECT_RETRY_DELAY;

    /** The delivery mode. */
    protected  int deliveryMode                     = DeliveryMode.PERSISTENT;

    protected MessageMode messageMode               = MessageMode.BYTE;

    public BaseJMSSender() {
    }

    /**
     * Instantiates a new base JMS sender.
     *
     * @param connectionString The connection string
     * @param connectionFactory The connection factory
     * @param sendQueueName The send queue name
     * @param env The environment parameters map
     */
    public BaseJMSSender(String connectionString, String connectionFactory, String sendQueueName, Hashtable<String, String> env){
        this.connectionString    = connectionString;
        this.connectionFactory   = connectionFactory;
        this.sendQueueName       = sendQueueName;
        this.env                 = env;
        log.info( String.format("factory: %s, server: %s, queue: %s, env: %s",connectionFactory, connectionString, sendQueueName, env) );
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#isUp()
     */
    @Override
    public boolean isUp(){
        return isConnected;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#getDeliveryMode()
     */
    @Override
    public int getDeliveryMode() {
        return deliveryMode;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#setDeliveryMode(int)
     */
    @Override
    public void setDeliveryMode(int deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#connect()
     */
    @Override
    public boolean connect() {
        if (conn != null) return  true;
        isConnected = false;
        int failCount = 0;
        QueueConnectionWrapper connectionWrapper = new QueueConnectionWrapper(env,connectionFactory);
        while ( failCount < retryCount ) {
            try {
                conn = connectionWrapper.createConnection();
                isConnected = true;
                return true;
            } catch (Exception e) {
                log.error("ClassCastException initializing connection to queue",e);
                failCount ++;
            }
            try {
                log.debug(String.format("failCount: %s sleep for %s ms",failCount, retryDelay) );
                Thread.sleep(retryDelay);
            } catch (InterruptedException e) {
                log.info("Sleep interrupted - exiting");
                break;
            }
        }
        if (failCount >= retryCount){
            log.error("Unable to connect to message queue after "+failCount +" attempts. Giving up!" );
        }
        log.info(String.format("Connected %s: %s",connectionFactory,isConnected));
        return isConnected;
    }

    @Override
    public boolean send(InputStream message) throws MessageSenderException {
        return send(message,null);
    }

    /* (non-Javadoc)
        * @see org.ccn2.common.queue.JMSSender#send(java.io.InputStream)
        */
    @Override
    public boolean send(InputStream message, Map<String,Object> headers) throws MessageSenderException {

        Session session = null;
        MessageProducer sender = null;
        boolean result = false;
        log.debug(String.format("JMSSender to queue: %s",sendQueueName) );
        try {
            if ( !connect() ) return false;

            try {
                session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
            } catch (JMSException e){
                disconnect();
                log.error("JMS connection in sender lost. Will try reconnect once...",e);
                if ( !connect() ) return false;
                session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
            }
            log.debug("JMS session created.");
            sender = session.createProducer( sendQueueName!=null?session.createQueue(sendQueueName):session.createTopic(sendTopicName) );
            sender.setDeliveryMode(deliveryMode);
            //sender.setDisableMessageID(true);
            log.debug("JMS sender created.");

            Message msg = messageMode == MessageMode.BYTE?
                            fillBytesMessage( session.createBytesMessage(), message ):
                            session.createTextMessage(StreamUtils.inputStreamToString(message) );
            applyHeaders(msg,headers);

            sender.send(msg);
            log.info(String.format("Message send to queue %s.",  sendQueueName));
            result = true;
        } catch (Exception e) {
            log.error("Exception pushing message to queue",e);
            throw new MessageSenderException(e);
        } finally {
            closeClosable(sender);
            closeClosable(session);
            //disconnect();
        }
        return result;
    }

    public boolean send(MessageModel model) throws MessageSenderException {
        this.messageMode = model.getMessageType().equals("TextMessage")?MessageMode.TEXT:MessageMode.BYTE;
        return send( model.getContentAsStream(), model.getHeaders() );
    }


    protected void applyHeaders(Message message, Map<String,Object> headers) throws JMSException {
        if ( headers == null ) return;
        for (String key: headers.keySet()){
            Object val = headers.get(key);
            if ( val == null ) continue;
            if ( key.startsWith("JMS") ){
                // ignore these properties
            } else if ( key.startsWith("_") ){
                // ignore these - used by vendors
//            } else if ( "JMSMessageId".equals(key) ){
//                message.setJMSMessageID(val.toString());
//            } else if ( "JMSCorrelationID".equals(key) ){
//                message.setJMSCorrelationID(val.toString());
//            } else if ("JMSTimestamp".equals(key)) {
//                try {
//                    long timestamp = Long.parseLong(val.toString());
//                    message.setJMSTimestamp(timestamp);
//                    return;
//                }
//                catch (NumberFormatException e) {
//                    throw new IllegalArgumentException("Invalid header for JMSTimestamp: "+ val);
//                }
            } else {
                message.setObjectProperty(key.replace("-",""),val);
            }
        }
    }





    /* (non-Javadoc)
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable {
        disconnect();
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#disconnect()
     */
    @Override
    public void disconnect(){
        conn = closeClosable(conn);
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#getRetryCount()
     */
    public int getRetryCount() {
        return retryCount;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#setRetryCount(int)
     */
    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#getRetryDelay()
     */
    public int getRetryDelay() {
        return retryDelay;
    }

    /* (non-Javadoc)
     * @see org.ccn2.common.queue.JMSSender#setRetryDelay(int)
     */
    public void setRetryDelay(int retryDelay) {
        this.retryDelay = retryDelay;
    }

    @Override
    public String getQueueName() {
        return sendQueueName;
    }

    @Override
    public void setQueueName(String sendQueueName) {
        this.sendQueueName = sendQueueName;
    }

    @Override
    public String getTopicName() {
        return sendTopicName;
    }

    @Override
    public void setTopicName(String sendTopicName) {
        this.sendTopicName = sendTopicName;
    }

    @Override
    public MessageMode getMessageMode() {
        return messageMode;
    }

    @Override
    public void setMessageMode(MessageMode messageMode) {
        this.messageMode = messageMode;
    }

    protected BytesMessage fillBytesMessage(BytesMessage message, InputStream inputStream) throws JMSException, IOException {
        int bytes_read, len = 0;
        byte [] buf = new byte[BUFLEN];
        while ((bytes_read = inputStream.read(buf)) != -1) {
            len += bytes_read;
            message.writeBytes(buf, 0, bytes_read);
        }

        message.setJMSReplyTo(null);
        message.reset();
        return message;
    }

    protected <T extends AutoCloseable> T closeClosable(T closeable){
        if ( closeable == null ) return null;
        try{
            closeable.close();
        } catch (Exception e) {
            log.warn("Cannot close {}. Ignore it.",closeable);
        }
        return null;
    }

}
