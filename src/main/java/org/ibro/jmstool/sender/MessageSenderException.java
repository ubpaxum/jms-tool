package org.ibro.jmstool.sender;

import org.ibro.jmstool.MessageException;

/**
 * Created by bily on 5/26/15.
 */
public class MessageSenderException extends MessageException {

    public MessageSenderException() {
        super();
    }

    public MessageSenderException(String message) {
        super(message);
    }

    public MessageSenderException(String message, Throwable cause) {
        super(message, cause);
    }

    public MessageSenderException(Throwable cause) {
        super(cause);
    }

    protected MessageSenderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
