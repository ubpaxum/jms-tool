package org.ibro.jmstool.test;

import org.ibro.jmstool.config.JmsToolConfig;
import org.ibro.jmstool.config.JmsConnectionSetting;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ToolConfigTest {

    @Test
    public void yamlSaveTest(){
        Map<String,JmsConnectionSetting> config = new HashMap<>();
        JmsConnectionSetting setting1 = new JmsConnectionSetting();
        setting1.setUrl("10.20.10.15:5545");
        setting1.setType("hornetq");

        JmsConnectionSetting setting2 = new JmsConnectionSetting();
        setting2.setUrl("10.20.10.15:5645");
        setting2.setType("context");
        setting2.setFactory("/ConnectionFactory");

        config.put("config1",setting1);
        config.put("config2",setting2);

        System.out.printf("%s\n",new Yaml().dumpAsMap(config));

    }

    @Test
    public void yamlLoadTest() throws Exception{
        String configFile = "jmstool.yaml";
        try (InputStream is = new FileInputStream( new File(configFile) )){
            JmsToolConfig config = new Yaml( ).loadAs(is,JmsToolConfig.class);
            System.out.printf("%s\n",new Yaml().dumpAsMap(config));
        }
    }

}
